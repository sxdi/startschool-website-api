<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Startschool\Exam\Models\Schedule as ScheduleModels;

class ScheduleTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(ScheduleModels $schedule)
    {
        $typeTrans   = new \Startschool\Api\Transformers\ExamTypeTransformer;
        $courseTrans = new \Startschool\Api\Transformers\CourseTransformer;
        return [
            'id'       => $exam->id,
            'name'     => $exam->name,
            'code'     => $exam->code,
            'type'     => $typeTrans->transform($exam->type),
            'course'   => $courseTrans->transform($exam->course),
            'question' => count($exam->items),
            'parameter'=> $exam->parameter
        ];
    }

    public function renderQuestion($exam)
    {
        $itemTrans   = new \Startschool\Api\Transformers\ExamItemTransformer;
        $data        = [];
        foreach ($exam->items as $item) {
            array_push($data, $itemTrans->transform($item));
        }

        return $data;
    }
}

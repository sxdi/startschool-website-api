<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

class WorkEssayItemTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(\Startschool\Work\Models\Item $item)
    {
        return [
            'id'        => $item->id,
            'question'  => $item->question,
            'answer'    => '',
            'parameter' => $item->parameter
        ];
    }
}

<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Startschool\Exam\Models\Temporary as TemporaryModels;

class ExamMapTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(TemporaryModels $item)
    {
        return [
            'id'       => $item->id,
            'question' => $item->question,
            'answer'   => $item->answer,
            'option'   => [
                'a' => $item->option_a,
                'b' => $item->option_b,
                'c' => $item->option_c,
                'd' => $item->option_d,
                'e' => $item->option_e,
            ],
        ];
    }
}

<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Startschool\Exam\Models\Item as ItemModels;

class ExamItemAnswerTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(ItemModels $item)
    {
        $examTrans = new \Startschool\Api\Transformers\ExamTransformer;
        return [
            'id'       => $item->id,
            'exam'     => $item->exam,
            'question' => $item->question,
            'answer'   => $item->answer,
            'parameter'=> $item->parameter,
            'option'   => [
                'a' => $item->option_a,
                'b' => $item->option_b,
                'c' => $item->option_c,
                'd' => $item->option_d,
                'e' => $item->option_e,
            ],
            'picture'   => [
                'question' => $item->picture ? $item->picture->path : (bool) false,
                'a'        => $item->optionA ? $item->optionA->path : (bool) false,
                'b'        => $item->optionB ? $item->optionB->path : (bool) false,
                'c'        => $item->optionC ? $item->optionC->path : (bool) false,
                'd'        => $item->optionD ? $item->optionD->path : (bool) false,
                'e'        => $item->optionE ? $item->optionE->path : (bool) false,
            ]
        ];
    }
}

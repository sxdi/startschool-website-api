<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

class EventTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(\Startschool\Event\Models\Event $event)
    {
        return [
            'id'        => $event->id,
            'date'      => $this->transformDate($event->date),
            'parameter' => $event->parameter
        ];
    }

    public function transformDate($date)
    {
        return [
            'dFY' => $date->format('d F Y'),
            'ymd' => $date->format('Y-m-d'),
            'Hi'  => $date->format('H:i')
        ];
    }
}

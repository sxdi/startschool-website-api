<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

class WorkResultTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(\Startschool\Work\Models\Result $result)
    {
        $work = \Startschool\Work\Models\Work::find($result->work_id);
        if($work) {
            return [
                'id'        => $result->id,
                'score'     => $result->score,
                'document'  => $this->transformDocument($result->document),
                'work'      => $this->transformWork($result->work_id),
                'student'   => $this->transformStudent($result->student_id),
                'create'    => $this->transformDate($result->created_at),
                'update'    => $this->transformDate($result->updated_at),
                'parameter' => $result->parameter,
            ];
        }

        return [];
    }

    public function transformWork($workId)
    {
        $work            = \Startschool\Work\Models\Work::find($workId);
        if($work) {
            $workTransformer = new \Startschool\Api\Transformers\WorkTransformer;
            return $workTransformer->transform($work);
        }

        return (bool) false;
    }

    public function transformDocument($document)
    {
        if($document) {
            return [
                'name' => $document->file_name,
                'size' => $document->size,
                'path' => $document->path
            ];
        }

        return (bool) false;
    }

    public function transformStudent($studentId)
    {
        $student = \Startschool\Student\Models\User::find($studentId);
        $studentTransformer = new \Startschool\Api\Transformers\StudentTransformer;
        return $studentTransformer->transform($student);
    }

    public function transformDate($value)
    {
        return [
            'dFY' => $value->format('d F Y'),
            'dFYHi' => $value->format('d F Y H:i'),
            'ymd' => $value->format('Y-m-d'),
        ];
    }
}

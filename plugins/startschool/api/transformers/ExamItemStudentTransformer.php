<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Startschool\Exam\Models\Student as StudentModels;

class ExamItemStudentTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(StudentModels $item)
    {
        return [
            'id'       => $item->id,
            'question' => $item->question,
            'answer'   => $item->answer ? $item->answer : (bool) false,
            'option'   => [
                'a' => $item->option_a,
                'b' => $item->option_b,
                'c' => $item->option_c,
                'd' => $item->option_d,
                'e' => $item->option_e,
            ]
        ];
    }
}

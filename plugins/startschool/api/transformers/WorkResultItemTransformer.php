<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

class WorkResultItemTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(\Startschool\Work\Models\ResultItem $item)
    {
        return [
            'id'       => $item->id,
            'question' => $item->question,
            'answer'   => $item->answer,
        ];
    }
}

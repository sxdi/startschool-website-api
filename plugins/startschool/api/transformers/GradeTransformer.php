<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Startschool\Grade\Models\Grade as GradeModels;

class GradeTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(GradeModels $grade)
    {

        return [
            'id'      => $grade->id,
            'name'    => $grade->name,
            'slug'    => $grade->slug,
            'student' => [
                'total'  => $grade->countStudent(),
                'male'   => $grade->countMaleStudent(),
                'female' => $grade->countFemaleStudent(),
            ]
        ];
    }
}

<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Startschool\Student\Models\User as StudentModels;

class StudentTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(StudentModels $student)
    {
        return [
            'id'    => $student->id,
            'nis'   => $student->nis,
            'nisn'  => $student->nisn,
            'name'  => $student->name,
            'code'  => $student->nisn
        ];
    }
}

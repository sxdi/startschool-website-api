<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

class EventItemTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(\Startschool\Event\Models\Item $item)
    {
        return [
            'id'        => $item->id,
            'order'     => $item->order,
            'grade'     => $this->transformGrade($item->grade),
            'learn'     => $item->learn,
            'work'      => $item->work,
            'student'   => $item->student,
            'parameter' => $item->parameter,
        ];
    }

    public function transformGrade($grade)
    {
        return [
            'id'   => $grade->id,
            'name' => $grade->name,
        ];
    }
}

<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Startschool\User\Models\User as UserModels;

class UserTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(UserModels $user)
    {
        return [
            'id'       => $user->id,
            'code'     => $user->code,
            'name'     => $this->transformName($user),
            'grade'    => $this->transformGrade($user),
            'is_admin' => (bool) $user->is_admin,
            'auth'     => [
                'teacher' => (bool) $user->teacher,
                'student' => (bool) $user->student
            ]
        ];
    }

    public function transformGrade($user)
    {
        if($user->student) {
            $grade = \Startschool\Grade\Models\Student::whereStudentId($user->student->id)->first();
            $g     = \Startschool\Grade\Models\Grade::find($grade->grade_id);
            return [
                'id'   => $g->id,
                'name' => $g->name
            ];
        }

        return (bool) false;
    }

    public function transformName($user)
    {
        if($user->teacher) {
            return $user->teacher->name;
        }

        if($user->student) {
            return $user->student->name;
        }
    }
}

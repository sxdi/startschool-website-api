<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

class WorkEssayTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(\Startschool\Work\Models\Essay $essay)
    {
        return [
            'id'        => $essay->id,
            'name'      => $essay->name,
            'course'    => $this->transformCourse($essay->course),
            'parameter' => $essay->parameter,
        ];
    }

    public function transformCourse($course)
    {
        return [
            'id'   => $course->id,
            'name' => $course->name
        ];
    }
}

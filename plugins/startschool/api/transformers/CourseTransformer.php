<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Startschool\Course\Models\Course as CourseModels;

class CourseTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(CourseModels $course)
    {
        $teacherTransformer = new \Startschool\Api\Transformers\TeacherTransformer;
        return [
            'id'      => $course->id,
            'name'    => $course->name,
        ];
    }
}

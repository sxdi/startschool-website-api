<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

class LearnScheduleTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(\Startschool\Learn\Models\Schedule $schedule)
    {
        return [
            'id'          => $schedule->id,
            'teacher'     => $this->transformTeacher($schedule->teacher),
            'grade'       => $this->transformGrade($schedule->grade),
            'course'      => $this->transformCourse($schedule->course),
            'status'      => $this->transformStatus($schedule->status),
            'name'        => $schedule->name,
            'code'        => $schedule->code,
            'description' => $schedule->description,
            'start'       => $this->transformDate($schedule->started_at),
            'end'         => $this->transformDate($schedule->ended_at),
            'documents'   => $this->transformDocument($schedule->documents),
            'parameter'   => $schedule->parameter
        ];
    }

    public function transformDocument($documents)
    {
        if(count($documents)) {
            $data = [];
            foreach ($documents as $key => $document) {
                $data[$key] = [
                    'name' => $document->file_name,
                    'size' => $document->size,
                    'type' => $document->content_type,
                    'path' => $document->path,
                ];
            }

            return $data;
        }

        return [];
    }

    public function transformTeacher($teacher)
    {
        return [
            'id'   => $teacher->id,
            'name' => $teacher->name,
        ];
    }

    public function transformGrade($grade)
    {
        return [
            'id' => $grade->id,
            'name' => $grade->name
        ];
    }

    public function transformCourse($course)
    {
        return [
            'id' => $course->id,
            'name' => $course->name
        ];
    }

    public function transformStatus($status)
    {
        switch ($status) {
            case 'wait':
                return [
                    'value' => 'wait',
                    'name'  => 'Belum Mulai'
                ];
                break;

            case 'active':
                return [
                    'value' => 'active',
                    'name'  => 'Sedang Berlangsung'
                ];
                break;

            case 'end':
                return [
                    'value' => 'end',
                    'name'  => 'Telah Berakhir'
                ];
                break;

            default:
                break;
        }
    }

    public function transformDate($date)
    {
        return [
            'dFYHi' => $date->format('d F Y H:i'),
            'dFY'   => $date->format('d F Y'),
            'ymd'   => $date->format('Y-m-d'),
            'ymdHi' => $date->format('Y-m-d H:i'),
            'Hi'    => $date->format('H:i'),
        ];
    }
}

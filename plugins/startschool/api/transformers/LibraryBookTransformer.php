<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

class LibraryBookTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(\Startschool\Library\Models\Book $book)
    {
        return [
            'id'        => $book->id,
            'title'     => $book->title,
            'grade'     => $book->grade,
            'path'      => $book->path,
            'publisher' => $book->publisher,
            'parameter' => $book->parameter,
        ];
    }
}

<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

class LearnStudentTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(\Startschool\Learn\Models\Student $student)
    {
        return [
            'id'      => $student->id,
            'create'  => $this->transformDate($student->created_at)
        ];
    }

    public function transformDate($date)
    {
        return [
            'dFYHi' => $date->format('d F Y H:i'),
            'dFY'   => $date->format('d F Y'),
            'ymd'   => $date->format('Y-m-d'),
            'ymdHi' => $date->format('Y-m-d H:i'),
            'Hi'    => $date->format('H:i'),
        ];
    }
}

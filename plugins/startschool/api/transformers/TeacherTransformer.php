<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Startschool\Teacher\Models\User as TeacherModels;

class TeacherTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(TeacherModels $teacher)
    {
        return [
            'id'       => $teacher->user_id,
            'nik'      => $teacher->nik,
            'name'     => $teacher->name,
            'is_admin' => (bool) $teacher->user->is_admin
        ];
    }
}

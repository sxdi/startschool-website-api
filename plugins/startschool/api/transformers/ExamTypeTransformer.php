<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Startschool\Exam\Models\Type as TypeModels;

class ExamTypeTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(TypeModels $type)
    {
        return [
            'id'   => $type->id,
            'name' => $type->name,
            'code' => $type->code,
        ];
    }
}

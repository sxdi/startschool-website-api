<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

class WorkTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(\Startschool\Work\Models\Work $work)
    {
        return [
            'id'          => $work->id,
            'name'        => $work->name,
            'description' => $work->description,
            'essay_id'    => $work->essay_id,
            'status'      => $this->transformStatus($work->status),
            'type'        => $this->transformType($work),
            'grade'       => $this->transformGrade($work),
            'teacher'     => $this->transformTeacher($work->teacher),
            'course'      => $this->transformCourse($work),
            'start'       => $this->transformStartDate($work),
            'end'         => $this->transformEndDate($work),
            'document'    => $this->transformDocument($work->document),
            'parameter'   => $work->parameter
        ];
    }

    public function transformTeacher($teacher)
    {
        return [
            'name' => $teacher->name
        ];
    }

    public function transformDocument($document)
    {
        if($document) {
            return [
                'path' => $document->path,
                'name' => $document->file_name
            ];
        }

        return (bool) false;
    }

    public function transformStatus($status)
    {
        switch ($status) {
            case 'wait':
                return [
                    'name' => 'Belum Mulai',
                    'value'=> 'wait'
                ];
                break;

            case 'active':
                return [
                    'name' => 'Sedang Berlangsung',
                    'value'=> 'active'
                ];

            case 'end':
                return [
                    'name' => 'Telah Berakhir',
                    'value'=> 'end'
                ];
                break;

            default:
                break;
        }
    }

    public function transformType($work)
    {
        switch ($work->type) {
            case 'document':
                return [
                    'name' => 'Mengerjakan Dokumen',
                    'value'=> 'document'
                ];
                break;

            case 'essay':
                return [
                    'name' => 'Mengerjakan Esai',
                    'value'=> 'essay'
                ];
                break;

            default:
                break;
        }
    }

    public function transformGrade($work)
    {
        return [
            'id'   => $work->grade->id,
            'name' => $work->grade->name
        ];
    }

    public function transformCourse($work)
    {
        return [
            'id'   => $work->course->id,
            'name' => $work->course->name
        ];
    }

    public function transformStartDate($work)
    {
        return [
            'dFY' => $work->started_at->format('d F Y'),
            'ymd' => $work->started_at->format('Y-m-d'),
        ];
    }

    public function transformEndDate($work)
    {
        return [
            'dFY' => $work->ended_at->format('d F Y'),
            'ymd' => $work->ended_at->format('Y-m-d'),
        ];
    }
}

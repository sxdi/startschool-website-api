<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Startschool\Exam\Models\Schedule as ScheduleModels;

class ExamScheduleTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(ScheduleModels $schedule)
    {
        $typeTrans   = new \Startschool\Api\Transformers\ExamTypeTransformer;
        $courseTrans = new \Startschool\Api\Transformers\CourseTransformer;
        $gradeTrans  = new \Startschool\Api\Transformers\GradeTransformer;
        return [
            'id'        => $schedule->id,
            'hour'      => $schedule->start->diffInMinutes($schedule->end),
            'type'      => $typeTrans->transform($schedule->type),
            'grade'     => $gradeTrans->transform($schedule->grade),
            'parameter' => $schedule->parameter,
            'status'=> [
                'start'  => (bool) $schedule->is_active,
                'end'    => (bool) $schedule->is_end
            ],
            'start' => [
                'date'        => $schedule->start->format('d/m/Y'),
                'day'         => $schedule->start->format('l'),
                'time'        => $schedule->start->format('H:i'),
                'times'       => $schedule->start->format('H:i:s'),
                'picker_date' => $schedule->start->format('Y-m-d'),
                'picker_time' => $schedule->start->format('Y-m-d H:i:s')
            ],
            'end'=> [
                'date'        => $schedule->end->format('d/m/y'),
                'day'         => $schedule->end->format('l'),
                'time'        => $schedule->end->format('H:i'),
                'times'       => $schedule->end->format('H:i:s'),
                'picker_date' => $schedule->end->format('Y-m-d'),
                'picker_time' => $schedule->end->format('Y-m-d H:i:s')
            ],
            'exam' => [
                'id'       => $schedule->exam->id,
                'question' => count($schedule->exam->items),
                'course'   => $courseTrans->transform($schedule->exam->course),
            ],
        ];
    }
}

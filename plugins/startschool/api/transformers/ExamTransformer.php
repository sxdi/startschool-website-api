<?php namespace Startschool\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Startschool\Exam\Models\Exam as ExamModels;

class ExamTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(ExamModels $exam)
    {
        $courseTrans = new \Startschool\Api\Transformers\CourseTransformer;
        return [
            'id'       => $exam->id,
            'name'     => $exam->name,
            'code'     => $exam->code,
            'course'   => $courseTrans->transform($exam->course),
            'question' => count($exam->items),
            'parameter'=> $exam->parameter
        ];
    }

    public function renderQuestion($exam)
    {
        $itemTrans   = new \Startschool\Api\Transformers\ExamItemTransformer;
        $data        = [];
        foreach ($exam->items as $item) {
            array_push($data, $itemTrans->transform($item));
        }

        return $data;
    }
}

<?php namespace Startschool\Api\Controllers;

use Startschool\Api\Classes\ApiController;

class MasterStudent extends ApiController
{
    public function get()
    {
        $students = \Startschool\Student\Models\User::orderBy('name')->paginate(30);
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithCollection($students, new \Startschool\Api\Transformers\StudentTransformer),
            'paginate'  => [
                'count'    => $students->count(),
                'total'    => $students->total(),
                'nextPage' => $students->nextPageUrl(),
            ]
        ]);
    }

    public function find()
    {
        $students = \Startschool\Student\Models\User::where('nisn', 'like', '%'.input('nisn').'%')->paginate(30);
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithCollection($students, new \Startschool\Api\Transformers\StudentTransformer),
            'paginate'  => [
                'count'    => $students->count(),
                'total'    => $students->total(),
                'nextPage' => $students->nextPageUrl(),
            ]
        ]);
    }

    public function reset()
    {
        $student        = \Startschool\Student\Models\User::find(post('student_id'));
        $user           = \Startschool\User\Models\User::find($student->user_id);
        $user->password = \Hash::make($user->code);
        $user->save();

        return response()->json([
            'result' => true
        ]);
    }
}

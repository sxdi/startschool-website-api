<?php namespace Startschool\Api\Controllers;

use \Carbon\Carbon;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Startschool\Api\Classes\ApiController;

class TeacherWork extends ApiController
{
    public function get($userId)
    {
        $user     = \Startschool\User\Models\User::find($userId);
        $teacher  = $user->teacher;
        $works    = \Startschool\Work\Models\Work::whereTeacherId($teacher->id)->get();
        return response()->json([
            'result'   => true,
            'response' => $this->respondWithCollection($works, new \Startschool\Api\Transformers\WorkTransformer)
        ]);
    }

    public function detail($userId)
    {
        $work = \Startschool\Work\Models\Work::whereParameter(input('parameter'))->first();
        return response()->json([
            'result'   => true,
            'response' => $this->respondWithItem($work, new \Startschool\Api\Transformers\WorkTransformer)
        ]);
    }

    public function save($userId)
    {
        $rules = [
            'started_at'  => 'required|date',
            'ended_at'    => 'required|date',
            'grade_id'    => 'required',
            'course_id'   => 'required',
            'type'        => 'required|in:document,essay',
            'name'        => 'required',
            'description' => 'required',
        ];
        $attributeNames = [
            'started_at'  => 'tanggal mulai',
            'ended_at'    => 'tanggal akhir',
            'grade_id'    => 'kelas',
            'course_id'   => 'pelajaran',
            'type'        => 'jenis',
            'name'        => 'tugas',
            'description' => 'deskripsi',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first(),
            ]);
        }

        if(post('type') == 'essay') {
            if(!post('essay_id')) {
                return response()->json([
                    'message' => 'Bidang pilihan essay tidak boleh kosong'
                ]);
            }
        }

        $user    = \Startschool\User\Models\User::find($userId);
        $teacher = $user->teacher;

        $grades = post('grade_id');
        if(count($grades)) {
            foreach ($grades as $grade) {
                $work    = \Startschool\Work\Models\Work::firstOrCreate([
                    'id' => post('id')
                ]);
                $work->teacher_id  = $teacher->id;
                $work->grade_id    = $grade;
                $work->course_id   = post('course_id');
                $work->started_at  = post('started_at');
                $work->ended_at    = post('ended_at');
                $work->type        = post('type');
                $work->essay_id    = post('essay_id');
                $work->name        = post('name');
                $work->description = post('description');
                $work->save();

                if(\Input::hasFile('document')) {
                    $file            = new \System\Models\File;
                    $file->data      = \Input::file('document');
                    $file->save();
                    $work->document()->add($file);
                }
            }
        }


        return response()->json([
            'result' => true
        ]);
    }

    public function delete()
    {
        $work    = \Startschool\Work\Models\Work::whereParameter(input('parameter'))->first();

        /**
         * Delete result & document file
        */
        $results = \Startschool\Work\Models\Result::whereWorkId($work->id)->get();
        foreach ($results as $result) {
            if($result->document) {
                $result->document->delete();
            }

            $result->delete();
        }

        /**
         * Delete document file
         */
        if($work->document) {
            $work->document->delete();
        }
        $work->delete();

        return response()->json([
            'result'    => true,
        ]);
    }

    public function result()
    {
        $work     = \Startschool\Work\Models\Work::whereParameter(input('parameter'))->first();

        $studentId= \Startschool\Grade\Models\Student::whereGradeId($work->grade_id)->pluck('student_id');
        $students = \Startschool\Student\Models\User::whereIn('id', $studentId)->orderBy('name')->get();

        $data = [];
        foreach ($students as $key => $student) {
            $result     = \Startschool\Work\Models\Result::whereWorkId($work->id)->whereStudentId($student->id)->first();
            if($result) {
                $dataResult = $this->respondWithItem($result, new \Startschool\Api\Transformers\WorkResultTransformer);
            }
            else {
                $dataResult = false;
            }

            $data[$key] = [
                'student' => $this->respondWithItem($student, new \Startschool\Api\Transformers\StudentTransformer),
                'result'  => $dataResult
            ];
        }


        return response()->json([
            'result'   => true,
            'response' => $this->respondWithItem($work, new \Startschool\Api\Transformers\WorkTransformer),
            'results'  => $data
        ]);
    }

    public function resultSave()
    {
        $scores = post('scores');
        if($scores) {
            foreach ($scores as $key => $score) {
                $result = \Startschool\Work\Models\Result::firstOrNew([
                    'work_id'    => post('work_id'),
                    'student_id' => $score['student'],
                ]);
                $result->score = $score['value'];
                $result->save();
            }
        }

        return response()->json([
            'result' => true,
        ]);
    }

    public function resultDetail()
    {
        $result = \Startschool\Work\Models\Result::whereParameter(input('parameter'))->first();
        $work   = \Startschool\Work\Models\Work::find($result->work_id);
        $items  = \Startschool\Work\Models\ResultItem::whereResultId($result->id)->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondWithItem($result, new \Startschool\Api\Transformers\WorkResultTransformer),
            'work'     => $this->respondWithItem($work, new \Startschool\Api\Transformers\WorkTransformer),
            'items'    => $this->respondWithCollection($items, new \Startschool\Api\Transformers\WorkResultItemTransformer)
        ]);
    }

    public function resultDownload()
    {
        $work     = \Startschool\Work\Models\Work::whereParameter(input('parameter'))->first();
        $grade    = \Startschool\Grade\Models\Grade::find($work->grade_id);
        $studentId= \Startschool\Grade\Models\Student::whereGradeId($work->grade_id)->pluck('student_id');
        $students = \Startschool\Student\Models\User::orderBy('name', 'asc')->whereIn('id', $studentId)->get();

        $spreadsheet = new Spreadsheet();
        $sheet       = $spreadsheet->getActiveSheet();

        foreach ($students as $key => $student) {
            $cell     = $key + 1;
            $isWorking= \Startschool\Work\Models\Result::whereWorkId($work->id)->whereStudentId($student->id)->first();
            $sheet->setCellValue('A'.$cell, strtoupper($student->name));
            $sheet->setCellValue('B'.$cell, $grade->name);

            if($isWorking) {
                if($isWorking->score) {
                    $sheet->setCellValue('C'.$cell, (string) $isWorking->score);
                }
                else {
                    $sheet->setCellValue('C'.$cell, 'Tidak ada nilai');
                }
            }
            else {
                $sheet->setCellValue('C'.$cell, 'Tidak mengerjakan');
            }
        }

        $generator = new \Startschool\Core\Classes\Generator;
        $writer    = new Xlsx($spreadsheet);
        $fileName  = $generator->make().'.xlsx';
        $path      = storage_path('app/media/work/'.$fileName);
        $writer->save($path);

        return response()->json([
            'result'   => true,
            'path'     => env('APP_URL').'storage/app/media/work/'.$fileName
        ]);
    }
}

<?php namespace Startschool\Api\Controllers;

use Startschool\Api\Classes\ApiController;

class TeacherAccount extends ApiController
{
    public function eventGet($userId)
    {
        $events  = \Startschool\Event\Models\Event::get();
        return response()->json([
            'result'   => true,
            'response' => $this->respondWithCollection($events, new \Startschool\Api\Transformers\EventTransformer)
        ]);
    }

    public function eventDetail($userId)
    {
        $user    = \Startschool\User\Models\User::find($userId);
        $teacher = $user->teacher;

        $event   = \Startschool\Event\Models\Event::whereParameter(input('parameter'))->first();
        $items   = \Startschool\Event\Models\Item::whereEventId($event->id)->whereTeacherId($teacher->id)->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondWithItem($event, new \Startschool\Api\Transformers\EventTransformer),
            'items'    => $this->respondWithCollection($items, new \Startschool\Api\Transformers\EventItemTransformer)
        ]);
    }

    public function eventSave($userId)
    {
        $user    = \Startschool\User\Models\User::find($userId);
        $teacher = $user->teacher;

        $events = post('event');
        if($events) {
            foreach ($events as $key => $ev) {
                if(!$ev['order']) {
                    return response()->json([
                        'message' => 'Bidang isian jam pengajaran agenda ke-'.($key+1).' wajib di isi'
                    ]);
                }

                if(!$ev['grade_id']) {
                    return response()->json([
                        'message' => 'Bidang isian kelas agenda ke-'.($key+1).' wajib di isi'
                    ]);
                }

                if(!$ev['learn']) {
                    return response()->json([
                        'message' => 'Bidang isian materi agenda ke-'.($key+1).' wajib di isi'
                    ]);
                }

                if(!$ev['work']) {
                    return response()->json([
                        'message' => 'Bidang isian tugas agenda ke-'.($key+1).' wajib di isi'
                    ]);
                }

                if(!$ev['student']) {
                    return response()->json([
                        'message' => 'Bidang isian siswa yang tidak hari agenda ke-'.($key+1).' wajib di isi'
                    ]);
                }
            }

            $event = \Startschool\Event\Models\Event::firstOrCreate([
                'id' => post('id') ?: null
            ]);
            $event->save();

            $itemIds = [];
            foreach ($events as $key => $ev) {
                $item = \Startschool\Event\Models\Item::firstOrCreate([
                    'id' => $ev['id'] ?: null
                ]);
                $item->teacher_id  = $teacher->id;
                $item->event_id    = $event->id;
                $item->order       = $ev['order'];
                $item->grade_id    = $ev['grade_id'];
                $item->learn       = $ev['learn'];
                $item->work        = $ev['work'];
                $item->student     = $ev['student'];
                $item->save();
                array_push($itemIds, $item->id);
            }
            \Startschool\Event\Models\Item::whereEventId($event->id)->whereTeacherId($teacher->id)->whereNotIn('id', $itemIds)->delete();
        }
        else {
            return response()->json([
                'message' => 'Tidak ada agenda'
            ]);
        }

        return response()->json([
            'result' => true
        ]);
    }
}

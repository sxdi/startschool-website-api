<?php namespace Startschool\Api\Controllers;

use Startschool\Api\Classes\ApiController;

class Election extends ApiController
{
	public function get()
	{
		$startDate = \Carbon\Carbon::createFromFormat('Y-m-d H:i','2020-11-13 08:00');
		$endDate   = \Carbon\Carbon::createFromFormat('Y-m-d H:i','2020-11-13 13:00');
		$check     = \Carbon\Carbon::now()->between($startDate,$endDate);
		return $check;
	}

	public function mine()
	{
		$osis = \DB::table('tmp_election')->whereUserId(input('user_id'))->whereType('osis')->first();
		$mpk  = \DB::table('tmp_election')->whereUserId(input('user_id'))->whereType('mpk')->first();

		return response()->json([
			'result'   => true,
			'response' => [
				'data' => [
					'osis' => $osis ?: false,
					'mpk'  => $mpk ?: false,
				]
			]
		]);
	}

    public function vote()
    {
    	$check = $this->get();
    	if(!$check) {
    		return response()->json([
    			'message' => 'Tidak dapat mengirim suara: pemilihan hanya dapat dilakukan pada jam 08:00 s/d 13:00'
    		]);
    	}

    	\DB::table('tmp_election')
		    ->updateOrInsert(
		        ['type'    => post('type'), 'user_id' => post('user_id')],
		        ['vote_id' => post('vote_id')]
		    );

		    // Pusher
	        $options = array(
	            'cluster' => 'ap1',
	            'useTLS' => true
	        );
	        $pusher = new \Pusher\Pusher(
	            '87e2f3d444a71620a632',
			    '47eda0ddda34495f9d9c',
			    '1103425',
			    $options
	        );
	        $data['message'] = 'hello world';
	        $pusher->trigger('my-channel', 'my-event', $data);

        return response()->json([
            'result'    => true,
        ]);
    }
}

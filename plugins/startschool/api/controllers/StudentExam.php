<?php namespace Startschool\Api\Controllers;

use Startschool\Api\Classes\ApiController;

class StudentExam extends ApiController
{
    public function exam($userId)
    {
        $user       = \Startschool\User\Models\User::find($userId);
        $grade      = $user->student->grade->grade;
        $schedules  = \Startschool\Exam\Models\Schedule::whereGradeId($grade->id)->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondWithCollection($schedules, new \Startschool\Api\Transformers\ExamScheduleTransformer)
        ]);
    }

    public function examDetail($userId)
    {
        $user    = \Startschool\User\Models\User::find($userId);
        $grade   = $user->student->grade->grade;
        $schedule= \Startschool\Exam\Models\Schedule::whereParameter(input('id'))->whereGradeId($grade->id)->first();
        $exam    = \Startschool\Exam\Models\Exam::find($schedule->exam_id);
        $time    = \Carbon\Carbon::now()->format('Y-m-d H:i');

        return response()->json([
            'result'   => true,
            'schedule' => $this->respondWithItem($schedule, new \Startschool\Api\Transformers\ExamScheduleTransformer),
            'response' => $this->respondWithItem($exam, new \Startschool\Api\Transformers\ExamTransformer)
        ]);
    }

    public function examStart($userId)
    {
        $user    = \Startschool\User\Models\User::find($userId);
        $grade   = $user->student->grade->grade;
        $schedule= \Startschool\Exam\Models\Schedule::whereParameter(post('id'))->whereGradeId($grade->id)->first();
        $exam    = \Startschool\Exam\Models\Exam::find($schedule->exam_id);
        $check   = \Startschool\Exam\Models\Student::whereExamId($exam->id)->whereStudentId($user->student->id)->count();
        $time    = \Carbon\Carbon::now()->format('Y-m-d H:i');

        // if($time >= $schedule->end->format('Y-m-d H:i')) {
        //     return response()->json([
        //         'message' => 'kelewat'
        //     ]);
        // }

        // if($time < $schedule->start->format('Y-m-d H:i')) {
        //     return response()->json([
        //         'message' => 'Belum saatnya'
        //     ]);
        // }

        $questions = $exam->items()->count();
        if($check <= $questions) {
            $items   = \Startschool\Exam\Models\Item::whereExamId($exam->id)->get()->shuffle();
            foreach ($items as $key => $item) {
                if($check < $questions) {
                    $stuExam             = new \Startschool\Exam\Models\Student;
                    $stuExam->exam_id    = $exam->id;
                    $stuExam->student_id = $user->student->id;
                    $stuExam->item_id    = $item->id;
                    $stuExam->question   = $item->question;
                    $stuExam->option_a   = $item->option_a;
                    $stuExam->option_b   = $item->option_b;
                    $stuExam->option_c   = $item->option_c;
                    $stuExam->option_d   = $item->option_d;
                    $stuExam->option_e   = $item->option_e;
                    $stuExam->key        = $item->answer;
                    $stuExam->save();
                    $check++;
                }
            }
        }

        $questions = \Startschool\Exam\Models\Student::whereStudentId($user->student->id)->get();
        return response()->json([
            'result'   => true,
            'response' => $this->respondWithCollection($questions, new \Startschool\Api\Transformers\ExamItemStudentTransformer)
        ]);
    }

    // public function examQuestion($userId)
    // {
    //     $user    = \Startschool\User\Models\User::find($userId);
    //     $student = $user->student;
    //     $grade   = $student->grade->grade;
    //     $schedule= \Startschool\Exam\Models\Schedule::whereParameter(input('id'))->whereGradeId($grade->id)->first();
    //     $exam    = \Startschool\Exam\Models\Exam::find($schedule->exam_id);
    //     $items   = \Startschool\Exam\Models\Student::whereExamId($exam->id)->whereStudentId($student->id)->get();
    //     $item    = \Startschool\Exam\Models\Student::whereExamId($exam->id)->whereStudentId($student->id)->whereNull('answer')->first();
    //     $complete = \Startschool\Exam\Models\Student::whereExamId($exam->id)->whereStudentId($student->id)->whereNotNull('answer')->count();

    //     if($items->count() == $complete) {
    //         $finish = true;
    //     }
    //     else {
    //         $finish = false;
    //     }

    //     if($item) {
    //         $item = $item;
    //     }
    //     else {
    //         $item = $items[0];
    //     }
    //     return response()->json([
    //         'result'  => true,
    //         'schedule'=> $this->respondWithItem($schedule, new \Startschool\Api\Transformers\ExamScheduleTransformer),
    //         'response'=> $this->respondWithCollection($items, new \Startschool\Api\Transformers\ExamItemStudentTransformer),
    //         'question'=> $this->respondWithItem($item, new \Startschool\Api\Transformers\ExamItemStudentTransformer),
    //         'time'    => $schedule->end->format('Y-m-d H:i:s'),
    //         'finish'  => (bool) $finish
    //     ]);
    // }

    public function examQuestion($userId)
    {
        $user    = \Startschool\User\Models\User::find($userId);
        $student = $user->student;
        $grade   = $student->grade->grade;
        $schedule= \Startschool\Exam\Models\Schedule::whereParameter(input('id'))->whereGradeId($grade->id)->first();

        return response()->json([
            'result'  => true,
            'schedule'=> $this->respondWithItem($schedule, new \Startschool\Api\Transformers\ExamScheduleTransformer),
            'time'    => $schedule->end->format('Y-m-d H:i:s'),
        ]);
    }

    public function examMassive()
    {
        $exams = post('question');
        foreach ($exams as $key => $exam) {
            $item         = \Startschool\Exam\Models\Student::find(post('question')[$key]);
            $item->answer = post('answer')[$key];
            $item->save();
        }

        return response()->json([
            'result' => true
        ]);
    }

    public function examAnswer()
    {
        $item         = \Startschool\Exam\Models\Student::find(post('id'));
        $item->answer = post('option');
        $item->save();

        return response()->json([
            'result' => true
        ]);
    }

    public function examEnd($userId)
    {
	return response()->json([
            'result' => true
        ]);

        $student             = \Startschool\Student\Models\User::find($userId);
        $grade               = $student->grade->grade;
        $schedule            = \Startschool\Exam\Models\Schedule::whereGradeId($grade->id)->whereParameter(post('id'))->first();
        $schedule->save();
        return response()->json([
            'result' => true
        ]);
    }
}

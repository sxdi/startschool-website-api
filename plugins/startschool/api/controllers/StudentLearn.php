<?php namespace Startschool\Api\Controllers;

use Startschool\Api\Classes\ApiController;

class StudentLearn extends ApiController
{
    public function schedule($userId)
    {
        $user       = \Startschool\User\Models\User::find($userId);
        $student    = $user->student;

	$date 	    = date('Y-m-d');
        $schedules  = \Startschool\Learn\Models\Schedule::orderBy('started_at', 'asc')->whereGradeId($student->grade->grade->id)->where('started_at', 'like', '%'.$date.'%')->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondWithCollection($schedules, new \Startschool\Api\Transformers\LearnScheduleTransformer)
        ]);
    }

    public function scheduleDetail($userId)
    {
        $user       = \Startschool\User\Models\User::find($userId);
        $student    = $user->student;

        $schedule   = \Startschool\Learn\Models\Schedule::whereParameter(input('parameter'))->first();
        $attendance = \Startschool\Learn\Models\Student::whereStudentId($student->id)->whereScheduleId($schedule->id)->first();

        if($attendance) {
            $dataAttendance = $this->respondWithItem($attendance, new \Startschool\Api\Transformers\LearnStudentTransformer);
        }
        else {
            $dataAttendance = (bool) false;
        }
        return response()->json([
            'result'        => true,
            'attendance'    => $dataAttendance,
            'response'      => $this->respondWithItem($schedule, new \Startschool\Api\Transformers\LearnScheduleTransformer)
        ]);
    }

    public function attendance($userId)
    {
        $user    = \Startschool\User\Models\User::find($userId);
        $student = $user->student;

        $schedule= \Startschool\Learn\Models\Student::firstOrNew([
            'schedule_id' => post('schedule_id'),
            'student_id'  => $student->id
        ]);
        $schedule->save();

        return response()->json([
            'result' => true
        ]);
    }

    public function async($userId)
    {
        $user    = \Startschool\User\Models\User::find($userId);
        $student = $user->student;

        $schedules = post('schedule');
        foreach ($schedules as $key => $schedule) {
            $sch     = \Startschool\Learn\Models\Schedule::find(post('schedule')[$key]);
            $time    = \Carbon\Carbon::parse($sch->started_at->format('Y-m-d').' '.post('time')[$key]);
            $schedule= \Startschool\Learn\Models\Student::firstOrNew([
                'schedule_id' => $sch->id,
                'student_id'  => $student->id,
            ]);
            $schedule->created_at = $time;
            $schedule->save();
        }

        return response()->json([
            'result' => true
        ]);
    }
}

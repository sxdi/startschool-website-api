<?php namespace Startschool\Api\Controllers;

use Startschool\Api\Classes\ApiController;

class Grade extends ApiController
{
    public function get()
    {
        $grades = \Startschool\Grade\Models\Grade::get();
        return response()->json([
            'result'   => true,
            'response' => $this->respondWithCollection($grades, new \Startschool\Api\Transformers\GradeTransformer)
        ]);
    }

    public function select()
    {
        $grades = \Startschool\Grade\Models\Grade::get();
        $allGrade = [];
        $all    = [];
        $code   = '';
        foreach ($grades as $key => $grade) {
            if($code != $grade->code) {
                $g = \Startschool\Grade\Models\Grade::whereCode($grade->code)->get()->pluck('id');
                // $g = (array) $g;
                // $l = implode(',', $g);
                array_push($allGrade, [
                    'name'  => 'Seluruh Kelas '.$grade->code,
                    'value' => $g
                ]);
            }

            array_push($allGrade, [
                'name' => $grade->name,
                'value'=> (array)$grade->id
            ]);
            $code = $grade->code;
        }

        return response()->json([
            'result'    => true,
            'response'  => $allGrade
        ]);
    }

    public function detail($slug)
    {
        $grade    = \Startschool\Grade\Models\Grade::whereSlug($slug)->first();
        $students = \Startschool\Student\Models\User::whereHas('grade', function($q) use ($grade) {
            $q->whereGradeId($grade->id);
        })->orderBy('name', 'asc')->get();

        $courseIds = \Startschool\Grade\Models\Course::whereGradeId($grade->id)->get()->pluck('course_id');
        $courses   = \Startschool\Course\Models\Course::whereIn('id', $courseIds)->orderBy('name', 'asc')->get();
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithItem($grade, new \Startschool\Api\Transformers\GradeTransformer),
            'students'  => $this->respondWithCollection($students, new \Startschool\Api\Transformers\StudentTransformer),
            'courses'   => $this->respondWithCollection($courses, new \Startschool\Api\Transformers\CourseTransformer),
        ]);
    }
}

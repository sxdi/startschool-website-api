<?php namespace Startschool\Api\Controllers;

use Startschool\Api\Classes\ApiController;

class MasterEvent extends ApiController
{
    public function get()
    {
        $events = \Startschool\Event\Models\Event::get();
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithCollection($events, new \Startschool\Api\Transformers\EventTransformer)
        ]);
    }

    public function detail()
    {
        $event  = \Startschool\Event\Models\Event::whereParameter(input('parameter'))->first();
        $grades = \Startschool\Grade\Models\Grade::get();
        $data   = [];

        // Grade
        foreach ($grades as $key => $grade) {
            $data[$key]['id']   = $grade->id;
            $data[$key]['name'] = $grade->name;

            $items = \Startschool\Event\Models\Item::whereGradeId($grade->id)->whereEventId($event->id)->get();
            if(count($items)) {
                foreach ($items as $keyItem => $item) {
                    $data[$key]['items'][$keyItem]['work']   = $item->work;
                    $data[$key]['items'][$keyItem]['learn']  = $item->learn;
                }
            }
            else {
                $data[$key]['items'] = [];
            }
        }

        return response()->json([
            'result'    => true,
            'response'  =>  $this->respondWithItem($event, new \Startschool\Api\Transformers\EventTransformer),
            'grades'    => ['data' => $data]
        ]);
    }

    public function download()
    {
        $event  = \Startschool\Event\Models\Event::whereParameter(input('parameter'))->first();
        $grades = \Startschool\Grade\Models\Grade::get();
        $data   = [];

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/media/master-event.docx'));

        // Grade
        foreach ($grades as $key => $grade) {
            $data[$key]['id']   = $grade->id;
            $data[$key]['name'] = $grade->name;

            $items = \Startschool\Event\Models\Item::whereGradeId($grade->id)->whereEventId($event->id)->get();
            if(count($items)) {
                foreach ($items as $keyItem => $item) {
                    $replacements[$key] = [
                        'event_date'    => $event->date->format('d F Y'),
                        'event_time'    => $item->order,
                        'event_grade'   => $grade->name,
                        'event_teacher' => $item->teacher->name,
                        'event_learn'   => $item->learn,
                        'event_work'    => $item->work,
                        'event_student' => $item->student,
                    ];
                }
            }
            else {
                $replacements[$key] =  [
                    'event_date'    => '-',
                    'event_time'    => '-',
                    'event_grade'   => $grade->name,
                    'event_teacher' => '-',
                    'event_learn'   => '-',
                    'event_work'    => '-',
                    'event_student' => '-',
                ];
            }
        }

        $templateProcessor->cloneRowAndSetValues('event_date', $replacements);

        $generator= new \Startschool\Core\Classes\Generator;
        $fileName = $generator->make().'.docx';
        $path     = storage_path('app/media/event/'.$fileName);
        $templateProcessor->saveAs($path);

        return response()->json([
            'result'    => true,
            'path'      => env('APP_URL').'storage/app/media/event/'.$fileName
        ]);
    }
}

<?php namespace Startschool\Api\Controllers;

use Startschool\Api\Classes\ApiController;

class Auth extends ApiController
{
    public function student()
    {
        $rules = [
            'code'      => 'required',
            'password'  => 'required',
        ];
        $attributeNames = [
            'code'      => 'NISN',
            'password'  => 'password',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user = \Startschool\User\Models\User::whereCode(post('code'))->first();
        if(!$user) {
            return response()->json([
                'message' => 'Pengguna tidak ditemukan'
            ]);
        }

        $checkPass = \Hash::check(post('password'), $user->password);
        if(!$checkPass) {
            return response()->json([
                'message' => 'Password tidak sesuai'
            ]);
        }

        return response()->json([
            'result'   => true,
            'response' => $this->respondWithItem($user, new \Startschool\Api\Transformers\UserTransformer)
        ]);
    }

    public function teacher()
    {
        $rules = [
            'code'      => 'required',
            'password'  => 'required',
        ];
        $attributeNames = [
            'code'      => 'NIK',
            'password'  => 'password',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user = \Startschool\User\Models\User::whereCode(post('code'))->first();
        if(!$user) {
            return response()->json([
                'message' => 'Pengguna tidak ditemukan'
            ]);
        }

        $checkPass = \Hash::check(post('password'), $user->password);
        if(!$checkPass) {
            return response()->json([
                'message' => 'Password tidak sesuai'
            ]);
        }

        if(!$user->teacher) {
            return response()->json([
                'message' => 'Anda bukan guru'
            ]);
        }

        return response()->json([
            'result'   => true,
            'response' => $this->respondWithItem($user, new \Startschool\Api\Transformers\UserTransformer)
        ]);
    }

    public function password()
    {
        $rules = [
            'password' => 'required',
            'new'      => 'required|min:6',
            'retype'   => 'required|min:6|same:new',
        ];
        $attributeNames = [
            'password' => 'password lama',
            'new'      => 'password baru',
            'retype'   => 'ulangi password',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user = \Startschool\User\Models\User::find(post('user_id'));
        if(!\Hash::check(post('password'), $user->password)) {
            return response()->json([
                'message' => 'Password lama tidak sesuai'
            ]);
        }

        $user->password = \Hash::make(strtolower(post('new')));
        $user->save();

        return response()->json([
            'result' => true,
        ]);
    }
}

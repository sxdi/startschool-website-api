<?php namespace Startschool\Api\Controllers;

use \Carbon\Carbon;

use Startschool\Api\Classes\ApiController;

class TeacherWorkEssay extends ApiController
{
    public function get($userId)
    {
        $user     = \Startschool\User\Models\User::find($userId);
        $teacher  = $user->teacher;
        $essays   = \Startschool\Work\Models\Essay::whereTeacherId($teacher->id)->get();
        return response()->json([
            'result'   => true,
            'response' => $this->respondWithCollection($essays, new \Startschool\Api\Transformers\WorkEssayTransformer)
        ]);
    }

    public function detail()
    {
        $essay = \Startschool\Work\Models\Essay::whereParameter(input('parameter'))->first();
        return response()->json([
            'result'  => true,
            'response'=> $this->respondWithItem($essay, new \Startschool\Api\Transformers\WorkEssayTransformer)
        ]);
    }

    public function save($userId)
    {
        $rules = [
            'name'      => 'required',
            'course_id' => 'required',
        ];
        $attributeNames = [
            'name'      => 'nama',
            'course_id' => 'pelajaran',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user  = \Startschool\User\Models\User::find($userId);
        $essay = \Startschool\Work\Models\Essay::firstOrCreate([
            'id' => post('id')
        ]);
        $essay->teacher_id = $user->teacher->id;
        $essay->name       = post('name');
        $essay->course_id  = post('course_id');
        $essay->save();

        return response()->json([
            'result' => true
        ]);
    }

    public function delete()
    {
        $essay = \Startschool\Work\Models\Essay::whereParameter(input('parameter'))->first();
        $items = \Startschool\Work\Models\Item::whereEssayId($essay->id)->get();

        if(count($items)) {
            $essay->items->delete();
        }
        $essay->delete();

        return response()->json([
            'result' => true
        ]);
    }


    /**
     * Item
    */
    public function item()
    {
        $essay = \Startschool\Work\Models\Essay::whereParameter(input('parameter'))->first();
        $items = \Startschool\Work\Models\Item::whereEssayId($essay->id)->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondWithCollection($items, new \Startschool\Api\Transformers\WorkEssayItemTransformer),
            'essay'    => $this->respondWithItem($essay, new \Startschool\Api\Transformers\WorkEssayTransformer),
        ]);
    }

    public function itemSave()
    {
        $questions = post('question');
        foreach ($questions as $key => $question) {
            if(!$question) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Kolom pertanyaan ke '.($key+1).' wajib di isi'
                ]);
            }
        }

        $ids   = [];
        $essay = \Startschool\Work\Models\Essay::find(post('essay_id'));
        foreach ($questions as $key => $question) {
            $items = \Startschool\Work\Models\Item::firstOrCreate([
                'id'       => post('id')[$key],
                'essay_id' => $essay->id
            ]);
            $items->question = $question;
            $items->save();
            array_push($ids, $items->id);
        }

        $delete = \Startschool\Work\Models\Item::whereEssayId($essay->id)->whereNotIn('id', $ids)->delete();

        return response()->json([
            'result' => true,
        ]);
    }
}

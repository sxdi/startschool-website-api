<?php namespace Startschool\Api\Controllers;

use Startschool\Api\Classes\ApiController;

class Student extends ApiController
{
    public function get()
    {
        $user    = \Startschool\User\Models\User::find(input('id'));
        $student = $user->student;

        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithItem($user, new \Startschool\Api\Transformers\UserTransformer)
        ]);
    }
}

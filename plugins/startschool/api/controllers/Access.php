<?php namespace Startschool\Api\Controllers;

use Startschool\Api\Classes\ApiController;

class Access extends ApiController
{
    public function get()
    {
        $teachers = \Startschool\Teacher\Models\User::orderBy('name', 'asc')->get();
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithCollection($teachers, new \Startschool\Api\Transformers\TeacherTransformer)
        ]);
    }

    public function detail($id)
    {
        $user            = \Startschool\User\Models\User::find($id);
        $teacher         = $user->teacher;

        $grades          = \Startschool\Grade\Models\Grade::get();
        $courses         = \Startschool\Course\Models\Course::orderBy('name', 'asc')->get();

        foreach ($grades as $key => $grade) {
            $accesses['grades'][$key]['id'] = $grade->id;
            $accesses['grades'][$key]['name'] = $grade->name;
        }

        foreach ($courses as $key => $course) {
            $accesses['courses'][$key]['id']   = $course->id;
            $accesses['courses'][$key]['name'] = $course->name;
        }

        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithItem($teacher, new \Startschool\Api\Transformers\TeacherTransformer),
            'accesses'  => $accesses
        ]);
    }
}

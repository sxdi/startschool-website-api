<?php namespace Startschool\Api\Controllers;

use Startschool\Api\Classes\ApiController;

class StudentWork extends ApiController
{
    public function get($userId)
    {
        $user       = \Startschool\User\Models\User::find($userId);
        $student    = $user->student;
        $grade      = \Startschool\Grade\Models\Student::whereStudentId($student->id)->first();
	$date 	    = date('Y-m-d');
        $works      = \Startschool\Work\Models\Work::orderBy('started_at', 'asc')->whereGradeId($grade->grade_id)->whereStatus('active')->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondWithCollection($works, new \Startschool\Api\Transformers\WorkTransformer)
        ]);
    }

    public function detail($userId)
    {
        $user   = \Startschool\User\Models\User::find($userId);
        $student= $user->student;
        $work   = \Startschool\Work\Models\Work::whereParameter(input('parameter'))->first();
        $result = \Startschool\Work\Models\Result::whereWorkId($work->id)->whereStudentId($student->id)->first();

        if($result) {
            $isWorking = $this->respondWithItem($result, new \Startschool\Api\Transformers\WorkResultTransformer);
        }
        else {
            $isWorking = ['data' => (bool) false];
        }
        return response()->json([
            'result'   => true,
            'response' => $this->respondWithItem($work, new \Startschool\Api\Transformers\WorkTransformer),
            'working'  => $isWorking
        ]);
    }

    public function result($userId)
    {
        $user    = \Startschool\User\Models\User::find($userId);
        $student = $user->student;

        $result  = \Startschool\Work\Models\Result::whereStudentId($student->id)->get();
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithCollection($result, new \Startschool\Api\Transformers\WorkResultTransformer)
	       // 'response'  => ['data' => []]
        ]);
    }

    public function submit($userId)
    {
        $user    = \Startschool\User\Models\User::find($userId);
        $student = $user->student;

        $work    = \Startschool\Work\Models\Work::whereParameter(input('parameter'))->first();
        $items   = \Startschool\Work\Models\Item::whereEssayId($work->essay_id)->get();
        $result  = \Startschool\Work\Models\Result::whereWorkId($work->id)->whereStudentId($student->id)->first();

        if($result) {
            $res = $this->respondWithItem($result, new \Startschool\Api\Transformers\WorkResultTransformer);
        }
        else {
            $res = ['data' => (bool) false];
        }
        return response()->json([
            'result'   => true,
            'response' => $this->respondWithItem($work, new \Startschool\Api\Transformers\WorkTransformer),
            'items'    => $this->respondWithCollection($items, new \Startschool\Api\Transformers\WorkEssayItemTransformer),
            'results'  => $res
        ]);
    }

    public function submitSave($userId)
    {
        $work    = \Startschool\Work\Models\Work::find(post('work_id'));

        if($work->type == 'essay') {
            $answers = post('answer');
            foreach ($answers as $key => $answer) {
                if(!post('answer')[$key]) {
                    return response()->json([
                        'message' => 'Jawaban untuk soal ke-'.($key+1).' wajib di isi'
                    ]);
                }
            }
        }

        if($work->type == 'document') {
            $rules = [
                'document' => 'required|file|max:50000',
            ];
            $attributeNames = [
                'document' => 'file',
            ];
            $messages  = [];
            $validator = \Validator::make(\Input::all(), $rules, $messages, $attributeNames);

            if($validator->fails()) {
                return response()->json([
                    'message' => $validator->messages()->first()
                ]);
            }
        }

        $user    = \Startschool\User\Models\User::find($userId);
        $student = $user->student;
        $items   = \Startschool\Work\Models\Item::whereEssayId($work->essay_id)->get();
        $result  = \Startschool\Work\Models\Result::firstOrCreate([
            'id' => post('id')
        ]);
        $result->work_id    = $work->id;
        $result->student_id = $student->id;
        $result->save();

        if($work->type == 'essay') {
            foreach ($items as $key => $item) {
                $submit = \Startschool\Work\Models\ResultItem::firstOrCreate([
                    'id' => post('id')
                ]);
                $submit->result_id= $result->id;
                $submit->item_id  = $item->id;
                $submit->question = $item->question;
                $submit->answer   = post('answer')[$key];
                $submit->save();
            }
        }

        if($work->type == 'document') {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('document');
            $file->save();
            $result->document()->add($file);
        }

        return response()->json([
            'result' => true
        ]);
    }
}

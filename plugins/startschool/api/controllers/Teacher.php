<?php namespace Startschool\Api\Controllers;

use \Carbon\Carbon;

use Startschool\Api\Classes\ApiController;

class Teacher extends ApiController
{
    public function get()
    {
        $user    = \Startschool\User\Models\User::find(input('id'));
        $teacher = $user->teacher;

        $courseId = \Startschool\Grade\Models\Course::whereIn('teacher_id', [$teacher->id])->pluck('course_id');
        $courses  = \Startschool\Course\Models\Course::whereIn('id', $courseId)->get();

        $gradeId  = \Startschool\Grade\Models\Course::whereIn('teacher_id', [$teacher->id])->pluck('grade_id');
        $grades   = \Startschool\Grade\Models\Grade::whereIn('id', $gradeId)->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondWithItem($teacher, new \Startschool\Api\Transformers\TeacherTransformer),
            'grades'   => $this->respondWithCollection($grades, new \Startschool\Api\Transformers\GradeTransformer),
            'courses'  => $this->respondWithCollection($courses, new \Startschool\Api\Transformers\CourseTransformer)
        ]);
    }
}

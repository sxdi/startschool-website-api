<?php namespace Startschool\Api\Controllers;

use \Carbon\Carbon;

use Startschool\Api\Classes\ApiController;

class TeacherExam extends ApiController
{
    /**
     * Schedule Examination
     * @param  [type] $userId [description]
     * @return [type]         [description]
    */
    public function result($userId)
    {
        $user       = \Startschool\User\Models\User::find($userId);
        $types      = \Startschool\Exam\Models\Type::get();
        $grades     = \Startschool\Grade\Models\Grade::whereIn('id', $user->getMyGradeId())->get();
        $courses    = \Startschool\Course\Models\Course::whereIn('id', $user->getMyCourseId())->get();
        $schedules  = \Startschool\Exam\Models\Schedule::whereIn('grade_id', $user->getMyGradeId());

        if(input('type_id')) {
            $inTypes   = explode(',', input('type_id'));
            $schedules = $schedules->whereIn('type_id', $inTypes);
        }
        else {
            $inTypes   = [];
        }

        if(input('grade_id')) {
            $inGrades  = explode(',', input('grade_id'));
            $schedules = $schedules->whereIn('grade_id', $inGrades);
        }
        else {
            $inGrades = [];
        }

        $inCourses = [];

        $schedules = $schedules->orderBy('created_at', 'desc')->whereIsEnd(1)->orWhere('is_active', '=', 1)->get();

        return response()->json([
            'result'    => true,
            'types'     => $this->respondWithCollection($types, new \Startschool\Api\Transformers\ExamTypeTransformer),
            'response'  => $this->respondWithCollection($schedules, new \Startschool\Api\Transformers\ExamScheduleTransformer),
            'grades'    => $this->respondWithCollection($grades, new \Startschool\Api\Transformers\GradeTransformer),
            'courses'   => $this->respondWithCollection($courses, new \Startschool\Api\Transformers\CourseTransformer),
            'filter'    => [
                'types'  => $inTypes,
                'grades' => $inGrades,
                'courses'=> $inCourses,
            ]
        ]);
    }

    public function schedule($userId)
    {
        $user       = \Startschool\User\Models\User::find($userId);
        $teacher    = $user->teacher;
        $types      = \Startschool\Exam\Models\Type::get();
        $grades     = \Startschool\Grade\Models\Grade::whereIn('id', $user->getMyGradeId())->get();
        $courses    = \Startschool\Course\Models\Course::whereIn('id', $user->getMyCourseId())->get();
        $schedules  = \Startschool\Exam\Models\Schedule::whereTeacherId($teacher->id);
        // $schedules  = \Startschool\Exam\Models\Schedule::whereIn('grade_id', $user->getMyGradeId());

        if(input('type_id')) {
            $inTypes   = explode(',', input('type_id'));
            $schedules = $schedules->whereIn('type_id', $inTypes);
        }
        else {
            $inTypes   = [];
        }

        if(input('grade_id')) {
            $inGrades  = explode(',', input('grade_id'));
            $schedules = $schedules->whereIn('grade_id', $inGrades);
        }
        else {
            $inGrades = [];
        }

        $inCourses = [];

        // $schedules = $schedules->orderBy('created_at', 'desc')->whereIsActive(0)->whereIsEnd(0)->get();
        $schedules = $schedules->orderBy('created_at', 'desc')->get();

        return response()->json([
            'result'    => true,
            'types'     => $this->respondWithCollection($types, new \Startschool\Api\Transformers\ExamTypeTransformer),
            'response'  => $this->respondWithCollection($schedules, new \Startschool\Api\Transformers\ExamScheduleTransformer),
            'grades'    => $this->respondWithCollection($grades, new \Startschool\Api\Transformers\GradeTransformer),
            'courses'   => $this->respondWithCollection($courses, new \Startschool\Api\Transformers\CourseTransformer),
            'filter'    => [
                'types'  => $inTypes,
                'grades' => $inGrades,
                'courses'=> $inCourses,
            ]
        ]);
    }

    public function scheduleDetail($userId)
    {
        $user     = \Startschool\User\Models\User::find($userId);
        $schedule = \Startschool\Exam\Models\Schedule::whereIn('grade_id', $user->getMyGradeId())->whereParameter(input('id'))->first();
        $students = \Startschool\Student\Models\User::whereHas('grade', function($q) use($schedule){
            $q->whereGradeId($schedule->grade->id);
        })->orderBy('name', 'asc')->get();

        $results = [];

        if($schedule->is_end) {
            foreach ($students as $keyStudent => $student) {
                $results[$keyStudent]['name'] = $student->name;

                $totalWrong = [];
                $totalTrue  = [];
                $totalAsnwer= [];
                foreach ($schedule->exam->items as $keyItem => $item) {
                    $isExist = \Startschool\Exam\Models\Student::whereStudentId($student->id)->whereItemId($item->id)->whereExamId($schedule->exam_id)->first();
                    if($isExist) {
                        if($isExist->key != $isExist->answer) {
                            array_push($totalWrong, '1');
                        }
                        else {
                            array_push($totalTrue, '1');
                        }

                        array_push($totalAsnwer, '1');
                    }
                }

                $results[$keyStudent]['result']['question'] = $schedule->exam->items->count();
                $results[$keyStudent]['result']['answered'] = count($totalAsnwer);
                $results[$keyStudent]['result']['wrong']    = count($totalWrong);
                $results[$keyStudent]['result']['true']     = count($totalTrue);
            }
        }

        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithItem($schedule, new \Startschool\Api\Transformers\ExamScheduleTransformer),
            'results'   => $results,
        ]);
    }

    public function scheduleActive($userId)
    {
        $user     = \Startschool\User\Models\User::find($userId);
        $schedules= \Startschool\Exam\Models\Schedule::whereIn('grade_id', $user->getMyGradeId())->whereIsActive(1)->get();
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithCollection($schedules, new \Startschool\Api\Transformers\ExamScheduleTransformer)
        ]);
    }

    public function scheduleGraph($userId)
    {
        $user     = \Startschool\User\Models\User::find($userId);
        $schedules= \Startschool\Exam\Models\Schedule::whereIn('grade_id', $user->getMyGradeId())->whereIsActive(1);

        if(input('id')) {
            $ids = explode(',', input('id'));
        }
        else {
            $ids = [];
        }

        $report    = [];
        $schedules =  $schedules->whereIn('id', $ids)->get();
        foreach ($schedules as $key => $schedule) {
            $studentIds            = $schedule->grade->getStudentId();
            $report[$key]['grade'] = [
                'id'     => $schedule->grade->id,
                'name'   => $schedule->grade->name,
            ];

            $students = \Startschool\Student\Models\User::orderBy('name', 'asc')->whereHas('grade', function($q) use($schedule) {
                $q->whereGradeId($schedule->grade->id);
            })->get();
            foreach ($students as $keyStudent => $student) {
                $question = \Startschool\Exam\Models\Student::whereStudentId($student->id)->whereExamId($schedule->exam_id)->count();
                $answered = \Startschool\Exam\Models\Student::whereStudentId($student->id)->whereExamId($schedule->exam_id)->whereNotNull('answer')->count();
                $report[$key]['grade']['students'][$keyStudent] = [
                    'name'      => $student->name,
                    'question'  => $question,
                    'answer'    => $answered
                ];
            }
        }

        return response()->json([
            'result'    => true,
            'response'  => $report
        ]);
    }

    public function scheduleAdd($userId)
    {
        $user   = \Startschool\User\Models\User::find($userId);
        $types  = \Startschool\Exam\Models\Type::get();
        $grades = \Startschool\Grade\Models\Grade::whereIn('id', $user->getMyGradeId())->get();
        $exams  = \Startschool\Exam\Models\Exam::whereIn('course_id', $user->getMyCourseId())->get();

        return response()->json([
            'result'    => true,
            'types'     => $this->respondWithCollection($types, new \Startschool\Api\Transformers\ExamTypeTransformer),
            'grades'    => $this->respondWithCollection($grades, new \Startschool\Api\Transformers\GradeTransformer),
            'exams'     => $this->respondWithCollection($exams, new \Startschool\Api\Transformers\ExamTransformer),
        ]);
    }

    public function scheduleEdit($userId)
    {
        $user     = \Startschool\User\Models\User::find($userId);
        $types    = \Startschool\Exam\Models\Type::get();
        $grades   = \Startschool\Grade\Models\Grade::whereIn('id', $user->getMyGradeId())->get();
        $exams    = \Startschool\Exam\Models\Exam::whereIn('course_id', $user->getMyCourseId())->get();
        $schedule = \Startschool\Exam\Models\Schedule::whereParameter(input('id'))->first();

        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithItem($schedule, new \Startschool\Api\Transformers\ExamScheduleTransformer),
            'types'     => $this->respondWithCollection($types, new \Startschool\Api\Transformers\ExamTypeTransformer),
            'grades'    => $this->respondWithCollection($grades, new \Startschool\Api\Transformers\GradeTransformer),
            'exams'     => $this->respondWithCollection($exams, new \Startschool\Api\Transformers\ExamTransformer),
        ]);
    }

    public function scheduleSave()
    {
        $rules = [
            'date'       => 'required',
            'start_time' => 'required',
            'end_time'   => 'required',
            'type_id'    => 'required',
            'grade_id'   => 'required',
            'exam_id'    => 'required',
        ];
        $attributeNames = [
            'date'       => 'tanggal',
            'start_time' => 'waktu mulai',
            'end_time'   => 'waktu berakhir',
            'type_id'    => 'jenis',
            'grade_id'   => 'kelas',
            'exam_id'    => 'soal',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $start_date = Carbon::createFromFormat('d/m/YH:i:s', post('date').post('start_time'));
        $end_date   = Carbon::createFromFormat('d/m/YH:i:s', post('date').post('end_time'));
        $teacher    = \Startschool\Teacher\Models\User::whereUserId(post('user_id'))->first();

        $schedule           = \Startschool\Exam\Models\Schedule::firstOrCreate([
            'id' => post('id')
        ]);
        $schedule->teacher_id = $teacher->id;
        $schedule->type_id    = post('type_id');
        $schedule->grade_id   = post('grade_id');
        $schedule->exam_id    = post('exam_id');
        $schedule->start      = Carbon::parse($start_date)->format('Y-m-d H:i:s');
        $schedule->end        = Carbon::parse($end_date)->format('Y-m-d H:i:s');
        $schedule->save();

        return response()->json([
            'result'    => true
        ]);
    }

    public function scheduleDelete()
    {
        $schedule = \Startschool\Exam\Models\Schedule::whereParameter(post('id'))->first();
        $schedule->delete();

        return response()->json([
            'result'    => true
        ]);
    }

    public function scheduleExcel($userId)
    {
        $user     = \Startschool\User\Models\User::find($userId);
        $schedule = \Startschool\Exam\Models\Schedule::whereIn('grade_id', $user->getMyGradeId())->whereParameter(input('id'))->first();
        $students = \Startschool\Student\Models\User::whereHas('grade', function($q) use($schedule){
            $q->whereGradeId($schedule->grade->id);
        })->orderBy('name', 'asc')->get();

        $results     = [];
        $reader      = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet = $reader;
        $reportSheet = $spreadsheet->setActiveSheetIndex(0);

        $styleHeader = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => '8eb4cb',
                ],
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'    => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ]
        ];
        $styleDate = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'    => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ]
        ];
        $styleBody = [
            'borders' => [
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];
        $styleBodyLast = [
            'borders' => [
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];

        $spreadsheet->getActiveSheet()->mergeCells('B2:B3');
        $spreadsheet->getActiveSheet()->getStyle('B2:B3')->applyFromArray($styleHeader);
        $reportSheet->getCell('B2')->setValue('NAMA');
        $spreadsheet->getActiveSheet()->mergeCells('C2:C3');
        $spreadsheet->getActiveSheet()->getStyle('C2:C3')->applyFromArray($styleHeader);
        $reportSheet->getCell('C2')->setValue('NIS');
        $spreadsheet->getActiveSheet()->mergeCells('D2:D3');
        $spreadsheet->getActiveSheet()->getStyle('D2:D3')->applyFromArray($styleHeader);
        $reportSheet->getCell('D2')->setValue('NISN');
        $spreadsheet->getActiveSheet()->mergeCells('E2:I2');
        $spreadsheet->getActiveSheet()->getStyle('E2:I2')->applyFromArray($styleHeader);
        $reportSheet->getCell('E2')->setValue('HASIL');
        $spreadsheet->getActiveSheet()->getStyle('E3')->applyFromArray($styleHeader);
        $reportSheet->getCell('E3')->setValue('SOAL');
        $spreadsheet->getActiveSheet()->getStyle('F3')->applyFromArray($styleHeader);
        $reportSheet->getCell('F3')->setValue('TERJAWAB');
        $spreadsheet->getActiveSheet()->getStyle('G3')->applyFromArray($styleHeader);
        $reportSheet->getCell('G3')->setValue('SALAH');
        $spreadsheet->getActiveSheet()->getStyle('H3')->applyFromArray($styleHeader);
        $reportSheet->getCell('H3')->setValue('BENAR');
        $spreadsheet->getActiveSheet()->getStyle('I3')->applyFromArray($styleHeader);
        $reportSheet->getCell('I3')->setValue('NILAI');

        $cell = 4;
        foreach ($students as $key => $student) {
            $key = $key + 1;
            $code= $student->nisn;
            $url = "sheet://'$code'!B3";

            // put name
            $spreadsheet->getActiveSheet()->getStyle('B'.$cell)->applyFromArray($styleBody);
            $reportSheet->getCell('B'.$cell)->setValue(strtoupper($student->name))->getHyperlink()->setUrl($url);

            $spreadsheet->getActiveSheet()->getStyle('C'.$cell)->applyFromArray($styleBody);
            $reportSheet->getCell('C'.$cell)->setValue($student->nis);

            $spreadsheet->getActiveSheet()->getStyle('D'.$cell)->applyFromArray($styleBody);
            $reportSheet->getCell('D'.$cell)->setValue($student->nisn);

            $totalWrong = [];
            $totalTrue  = [];
            $totalAsnwer= [];

            $worksheet1 = $spreadsheet->createSheet();
            $worksheet1->setTitle((string)$student->nisn);
            $studentSheet = $spreadsheet->setActiveSheetIndex($key);

            $spreadsheet->getActiveSheet()->getStyle('B2')->applyFromArray($styleHeader);
            $studentSheet->getCell('B2')->setValue('NOMOR');
            $spreadsheet->getActiveSheet()->getStyle('C2')->applyFromArray($styleHeader);
            $studentSheet->getCell('C2')->setValue('PERTANYAAN');
            $spreadsheet->getActiveSheet()->getStyle('D2')->applyFromArray($styleHeader);
            $studentSheet->getCell('D2')->setValue('PILIHAN A');
            $spreadsheet->getActiveSheet()->getStyle('E2')->applyFromArray($styleHeader);
            $studentSheet->getCell('E2')->setValue('PILIHAN B');
            $spreadsheet->getActiveSheet()->getStyle('F2')->applyFromArray($styleHeader);
            $studentSheet->getCell('F2')->setValue('PILIHAN C');
            $spreadsheet->getActiveSheet()->getStyle('G2')->applyFromArray($styleHeader);
            $studentSheet->getCell('G2')->setValue('PILIHAN D');
            $spreadsheet->getActiveSheet()->getStyle('H2')->applyFromArray($styleHeader);
            $studentSheet->getCell('H2')->setValue('PILIHAN E');
            $spreadsheet->getActiveSheet()->getStyle('I2')->applyFromArray($styleHeader);
            $studentSheet->getCell('I2')->setValue('KUNCI');
            $spreadsheet->getActiveSheet()->getStyle('J2')->applyFromArray($styleHeader);
            $studentSheet->getCell('J2')->setValue('JAWABAN');
            $spreadsheet->getActiveSheet()->getStyle('K2')->applyFromArray($styleHeader);
            $studentSheet->getCell('K2')->setValue('HASIL');
            $cellResume = 3;
            foreach ($schedule->exam->items as $keyItem => $item) {
                $isExist = \Startschool\Exam\Models\Student::whereStudentId($student->id)->whereItemId($item->id)->whereExamId($schedule->exam_id)->first();
                // $studentSheet->getCell('B'.$cellResume)->setValue((string)$keyItem+1);
                $spreadsheet->getActiveSheet()->getStyle('B'.$cellResume)->applyFromArray($styleBody);
                $studentSheet->getCell('C'.$cellResume)->setValue($item->question);
                $spreadsheet->getActiveSheet()->getStyle('C'.$cellResume)->applyFromArray($styleBody);
                $studentSheet->getCell('D'.$cellResume)->setValue($item->option_a);
                $spreadsheet->getActiveSheet()->getStyle('D'.$cellResume)->applyFromArray($styleBody);
                $studentSheet->getCell('E'.$cellResume)->setValue($item->option_b);
                $spreadsheet->getActiveSheet()->getStyle('E'.$cellResume)->applyFromArray($styleBody);
                $studentSheet->getCell('F'.$cellResume)->setValue($item->option_c);
                $spreadsheet->getActiveSheet()->getStyle('F'.$cellResume)->applyFromArray($styleBody);
                $studentSheet->getCell('G'.$cellResume)->setValue($item->option_d);
                $spreadsheet->getActiveSheet()->getStyle('G'.$cellResume)->applyFromArray($styleBody);
                $studentSheet->getCell('H'.$cellResume)->setValue($item->option_e);
                $spreadsheet->getActiveSheet()->getStyle('H'.$cellResume)->applyFromArray($styleBody);
                $studentSheet->getCell('I'.$cellResume)->setValue($item->answer);
                $spreadsheet->getActiveSheet()->getStyle('I'.$cellResume)->applyFromArray($styleBody);


                if($isExist) {
                    $studentSheet->getCell('J'.$cellResume)->setValue($isExist->answer);
                    $spreadsheet->getActiveSheet()->getStyle('J'.$cellResume)->applyFromArray($styleBody);
                    array_push($totalAsnwer, '1');

                    if($isExist->key != $isExist->answer) {
                        $studentSheet->getCell('K'.$cellResume)->setValue('SALAH');
                        $spreadsheet->getActiveSheet()->getStyle('K'.$cellResume)->applyFromArray($styleBody);
                        array_push($totalWrong, '1');
                    }
                    else {
                        $studentSheet->getCell('K'.$cellResume)->setValue('BENAR');
                        $spreadsheet->getActiveSheet()->getStyle('K'.$cellResume)->applyFromArray($styleBody);
                        array_push($totalTrue, '1');
                    }
                }
                else {
                    $studentSheet->getCell('J'.$cellResume)->setValue('Tidak terjawab');
                    $spreadsheet->getActiveSheet()->getStyle('J'.$cellResume)->applyFromArray($styleBody);
                }

                $cellResume++;
            }

            $reportSheet = $spreadsheet->setActiveSheetIndex(0);
            $spreadsheet->getActiveSheet()->getStyle('E'.$cell)->applyFromArray($styleBody);
            $reportSheet->getCell('E'.$cell)->setValue((string) $schedule->exam->items->count());
            $spreadsheet->getActiveSheet()->getStyle('F'.$cell)->applyFromArray($styleBody);
            $reportSheet->getCell('F'.$cell)->setValue((string) count($totalAsnwer));
            $spreadsheet->getActiveSheet()->getStyle('G'.$cell)->applyFromArray($styleBody);
            $reportSheet->getCell('G'.$cell)->setValue((string) count($totalWrong));
            $spreadsheet->getActiveSheet()->getStyle('H'.$cell)->applyFromArray($styleBody);
            $reportSheet->getCell('H'.$cell)->setValue((string) count($totalTrue));
            $spreadsheet->getActiveSheet()->getStyle('I'.$cell)->applyFromArray($styleBody);
            $reportSheet->getCell('I'.$cell)->setValue((string) ceil(count($totalTrue)/$schedule->exam->items->count()*100));
            $cell++;
        }

        $generator = new \Startschool\Core\Classes\Generator;
        $filename  = $generator->make().'.xls';
        $tmp       = storage_path('app/media/'.$filename);
        $writer    = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save($tmp);

        return response()->json([
            'result'    => true,
            'response'  => env('APP_URL').'storage/app/media/'.$filename
        ]);
    }



    /**
     * Examination
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function exam($userId)
    {
        $user   = \Startschool\User\Models\User::find($userId);
    	$exams  = \Startschool\Exam\Models\Exam::whereIn('course_id', $user->getMyCourseId())->get();
    	return response()->json([
    		'result'	=> true,
    		'response'	=> $this->respondWithCollection($exams, new \Startschool\Api\Transformers\ExamTransformer),
    	]);
    }

    public function examEdit($userId)
    {
        $user   = \Startschool\User\Models\User::find($userId);
        $exam   = \Startschool\Exam\Models\Exam::whereParameter(input('id'))->first();
        $courses= \Startschool\Course\Models\Course::whereIn('id', $user->getMyCourseId())->get();
        $types  = \Startschool\Exam\Models\Type::get();
        return response()->json([
            'result'   => true,
            'response' => $this->respondWithItem($exam, new \Startschool\Api\Transformers\ExamTransformer),
            'courses'  => $this->respondWithCollection($courses, new \Startschool\Api\Transformers\CourseTransformer),
            'types'    => $this->respondWithCollection($types, new \Startschool\Api\Transformers\ExamTypeTransformer),
        ]);
    }

    public function examEditSave()
    {
        $exam   = \Startschool\Exam\Models\Exam::whereParameter(post('id'))->first();
        $rules = [
            'code'      => 'required',
            'course_id' => 'required',
        ];
        $attributeNames = [
            'code'      => 'kode',
            'course_id' => 'mata pelajaran',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $exam            = \Startschool\Exam\Models\Exam::find(post('id'));
        $exam->code      = post('code');
        $exam->course_id = post('course_id');
        $exam->save();

        return response()->json([
            'result' => true
        ]);
    }

    public function examDelete()
    {
        //$exam   = \Startschool\Exam\Models\Exam::whereParameter(post('id'))->first();
        //$exam->items()->delete();
        //$exam->delete();
        return response()->json([
            'result' => true
        ]);
    }

    public function examImport($userId)
    {
        $user   = \Startschool\User\Models\User::find($userId);
        $courses= \Startschool\Course\Models\Course::whereIn('id', $user->getMyCourseId())->get();
        return response()->json([
            'result'    => true,
            'courses'   => $this->respondWithCollection($courses, new \Startschool\Api\Transformers\CourseTransformer),
        ]);
    }

    public function examImportSave($userId)
    {
        if(!\Input::hasFile('excelFile')) {
            return response()->json([
                'message' => 'Bidang isian file harus di isi'
            ]);
        }

        $rules = [
            'name'      => 'required',
            'course_id' => 'required',
        ];
        $attributeNames = [
            'name'      => 'kode soal',
            'course_id' => 'pelajaran',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $teacher         = \Startschool\Teacher\Models\User::whereUserId(post('user_id'))->first();
        $user            = \Startschool\User\Models\User::find($userId);
        $exam            = new \Startschool\Exam\Models\Exam;
        $exam->code      = post('name');
        $exam->course_id = post('course_id');
        $exam->save();

        if(\Input::hasFile('excelFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('excelFile');
            $file->save();
            $exam->document()->add($file);
        }

        $import      = \Excel::import(new \Startschool\Core\Classes\ExamImport, $exam->document->getLocalPath());
        $temporaries = \Startschool\Exam\Models\Temporary::whereNotNull('question')->get();
        foreach ($temporaries as $key => $temporary) {
            $temporary->teacher_id = $teacher->id;
            $temporary->save();
        }


        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithItem($exam, new \Startschool\Api\Transformers\ExamTransformer)
        ]);
    }

    public function examMap($userId)
    {
        $teacher   = \Startschool\Teacher\Models\User::whereUserId($userId)->first();
        $exam      = \Startschool\Exam\Models\Exam::whereParameter(input('id'))->first();
        $temporary = \Startschool\Exam\Models\Temporary::whereNotNull('question')->whereTeacherId($teacher->id)->first();

        if(!$temporary) {
            $isFinish = (bool) true;
        }
        else {
            $isFinish = (bool) false;
        }

        if($isFinish) {
             return response()->json([
                'result'    => true,
                'finish'    => true
            ]);
        }
        else {
            return response()->json([
                'result'    => true,
                'exam'      => $this->respondWithItem($exam, new \Startschool\Api\Transformers\ExamTransformer),
                'response'  => $this->respondWithItem($temporary, new \Startschool\Api\Transformers\ExamMapTransformer)
            ]);
        }
    }

    public function examMapSave()
    {
        $temporary      = \Startschool\Exam\Models\Temporary::find(post('id'));
        $item           = new \Startschool\Exam\Models\Item;
        $item->exam_id  = post('exam_id');
        $item->question = $temporary->question;
        $item->option_a = $temporary->option_a;
        $item->option_b = $temporary->option_b;
        $item->option_c = $temporary->option_c;
        $item->option_d = $temporary->option_d;
        $item->option_e = $temporary->option_e;
        $item->answer   = $temporary->answer;
        $item->save();

        $temporary->delete();

        return response()->json([
            'result'    => true
        ]);
    }

    public function examMapDelete()
    {
        $temporary      = \Startschool\Exam\Models\Temporary::find(post('id'));
        $temporary->delete();

        return response()->json([
            'result' => true
        ]);
    }

    public function examDetail()
    {
        $exam      = \Startschool\Exam\Models\Exam::whereParameter(input('id'))->first();
        $questions = \Startschool\Exam\Models\Item::whereExamId($exam->id)->get();
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithItem($exam, new \Startschool\Api\Transformers\ExamTransformer),
            'questions' => $this->respondWithCollection($questions, new \Startschool\Api\Transformers\ExamItemAnswerTransformer)
        ]);
    }

    public function examQuestionDetail()
    {
        $question = \Startschool\Exam\Models\Item::whereParameter(input('id'))->first();
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithItem($question, new \Startschool\Api\Transformers\ExamItemTransformer),
            'answer'    => $question->answer
        ]);
    }

    public function examQuestionSave()
    {
        $rules = [
            'question'      => 'required',
            'option_a'      => 'required',
            'option_b'      => 'required',
            'option_c'      => 'required',
            'option_d'      => 'required',
            'option_e'      => 'required',
            'answer'        => 'required|in:option_a,option_b,option_c,option_d,option_e',
        ];
        $attributeNames = [
            'question'      => 'soal',
            'option_a'      => 'pilihan a',
            'option_b'      => 'pilihan b',
            'option_c'      => 'pilihan c',
            'option_d'      => 'pilihan d',
            'option_e'      => 'pilihan e',
            'answer'        => 'jawaban',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $question = \Startschool\Exam\Models\Item::whereParameter(input('id'))->first();
        $question->question = post('question');
        $question->option_a = post('option_a');
        $question->option_b = post('option_b');
        $question->option_c = post('option_c');
        $question->option_d = post('option_d');
        $question->option_e = post('option_e');

        switch (post('answer')) {
            case 'option_a':
                $answer = post('option_a');
                break;

            case 'option_b':
                $answer = post('option_b');
                break;

            case 'option_c':
                $answer = post('option_c');
                break;

            case 'option_d':
                $answer = post('option_d');
                break;

            case 'option_e':
                $answer = post('option_e');
                break;

            default:
                break;
        }

        $question->answer = $answer;
        $question->save();

        if(\Input::hasFile('picture')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('picture');
            $file->save();
            $question->picture()->add($file);
        }

        if(\Input::hasFile('optionA')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('optionA');
            $file->save();
            $question->optionA()->add($file);
        }

        if(\Input::hasFile('optionB')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('optionB');
            $file->save();
            $question->optionB()->add($file);
        }

        if(\Input::hasFile('optionC')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('optionC');
            $file->save();
            $question->optionC()->add($file);
        }

        if(\Input::hasFile('optionD')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('optionD');
            $file->save();
            $question->optionD()->add($file);
        }

        if(\Input::hasFile('optionE')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('optionE');
            $file->save();
            $question->optionE()->add($file);
        }

        return response()->json([
            'result' => true
        ]);
    }

    public function examQuestionImageSave()
    {
        $question = \Startschool\Exam\Models\Item::whereParameter(input('id'))->first();
        if(\Input::hasFile('picture')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('picture');
            $file->save();
            $question->picture()->add($file);
            $question->question = '<img src='.$question->picture->path.'>'.$question->question;
            $question->save();
        }

        if(\Input::hasFile('optionA')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('optionA');
            $file->save();
            $question->optionA()->add($file);
            $question->option_a = '<img src='.$question->optionA->path.'>'.$question->option_a;
            $question->save();
        }

        if(\Input::hasFile('optionB')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('optionB');
            $file->save();
            $question->optionB()->add($file);
            $question->option_b = '<img src='.$question->optionB->path.'>'.$question->option_b;
            $question->save();
        }

        if(\Input::hasFile('optionC')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('optionC');
            $file->save();
            $question->optionC()->add($file);
            $question->option_c = '<img src='.$question->optionC->path.'>'.$question->option_c;
            $question->save();
        }

        if(\Input::hasFile('optionD')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('optionD');
            $file->save();
            $question->optionD()->add($file);
            $question->option_d = '<img src='.$question->optionD->path.'>'.$question->option_d;
            $question->save();
        }

        if(\Input::hasFile('optionE')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('optionE');
            $file->save();
            $question->optionE()->add($file);
            $question->option_e = '<img src='.$question->optionE->path.'>'.$question->option_e;
            $question->save();
        }

        return response()->json([
            'result'    => true,
        ]);
    }
}

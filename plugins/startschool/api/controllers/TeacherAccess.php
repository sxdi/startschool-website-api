<?php namespace Startschool\Api\Controllers;

use Startschool\Api\Classes\ApiController;

class TeacherAccess extends ApiController
{
    public function get($id)
    {
        $user     = \Startschool\User\Models\User::find($id);
        $teacher  = $user->teacher;
        $accesses = [];

        $grades   = \Startschool\Grade\Models\Grade::get();
        foreach ($grades as $key => $grade) {
            $accesses['grades'][$key]['id']   = $grade->id;
            $accesses['grades'][$key]['name'] = $grade->name;
        }

        $courses = \Startschool\Course\Models\Course::orderBy('name', 'asc')->get();
        foreach ($courses as $key => $course) {
            $accesses['courses'][$key]['id']   = $course->id;
            $accesses['courses'][$key]['name'] = $course->name;
        }

        /**
         * Mine
         * 1) Grade
         * 2) Course
         * 3) Exam
        */
        $grades = \Startschool\User\Models\AccessGrade::whereUserId($user->id)->get()->pluck('grade_id');
        $courses= \Startschool\User\Models\AccessCourse::whereUserId($user->id)->get()->pluck('course_id');
        $accesses['result']['grades']  = $grades;
        $accesses['result']['courses'] = $courses;

        return response()->json([
            'result'    => true,
            'user'      => $this->respondWithItem($user->teacher, new \Startschool\Api\Transformers\TeacherTransformer),
            'response'  => $accesses
        ]);
    }

    public function save($userId)
    {
        $user = \Startschool\User\Models\User::find($userId);

        // grades
        if(post('grade_id')) {
            $grades = explode(',', post('grade_id'));
            $user->accessGrades()->sync($grades);
        }

        // courses
        if(post('course_id')) {
            $courses = explode(',', post('course_id'));
            $user->accessCourses()->sync($courses);
        }

        return response()->json([
            'result'    => true
        ]);
    }
}

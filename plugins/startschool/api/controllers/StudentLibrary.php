<?php namespace Startschool\Api\Controllers;

use \Carbon\Carbon;

use Startschool\Api\Classes\ApiController;

class StudentLibrary extends ApiController
{
    public function book($userId)
    {
        $user    = \Startschool\User\Models\User::find($userId);
        $student = $user->student;

        $gradeId = \Startschool\Grade\Models\Student::whereStudentId($student->id)->first();
        $grade   = \Startschool\Grade\Models\Grade::find($gradeId)->first();
        $books   = \Startschool\Library\Models\Book::where('grade', '=', $grade->group)->get();
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithCollection($books, new \Startschool\Api\Transformers\LibraryBookTransformer)
        ]);
    }
}

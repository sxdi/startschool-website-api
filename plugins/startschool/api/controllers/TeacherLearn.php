<?php namespace Startschool\Api\Controllers;

use \Carbon\Carbon;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Startschool\Api\Classes\ApiController;

class TeacherLearn extends ApiController
{
    public function get($userId)
    {
        $user      = \Startschool\User\Models\User::find($userId);
        $teacher   = $user->teacher;
        $schedules = \Startschool\Learn\Models\Schedule::whereTeacherId($teacher->id)->get();

        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithCollection($schedules, new \Startschool\Api\Transformers\LearnScheduleTransformer)
        ]);
    }

    public function scheduleDetail()
    {
        $schedule = \Startschool\Learn\Models\Schedule::whereParameter(input('parameter'))->first();
        return response()->json([
            'result' => true,
            'response' => $this->respondWithItem($schedule, new \Startschool\Api\Transformers\LearnScheduleTransformer)
        ]);
    }

    public function scheduleAttendance()
    {
        $schedule = \Startschool\Learn\Models\Schedule::whereParameter(input('parameter'))->first();
        $studentId= \Startschool\Grade\Models\Student::whereGradeId($schedule->grade_id)->pluck('student_id');
        $students = \Startschool\Student\Models\User::orderBy('name', 'asc')->whereIn('id', $studentId)->get();

        $dataStudent = [];
        foreach ($students as $key => $student) {
            $isAttendance              = \Startschool\Learn\Models\Student::whereScheduleId($schedule->id)->whereStudentId($student->id)->first();
            $dataStudent[$key]['name'] = strtoupper($student->name);
            if($isAttendance) {
                $dataStudent[$key]['create'] = $isAttendance->created_at->format('H:i');
            }
            else {
                $dataStudent[$key]['create'] = (bool) false;
            }
        }
        return response()->json([
            'result'   => true,
            'response' => ['data' => $dataStudent],
            'schedule' => $this->respondWithItem($schedule, new \Startschool\Api\Transformers\LearnScheduleTransformer)
        ]);
    }

    public function scheduleAttendanceDownload()
    {
        $schedule = \Startschool\Learn\Models\Schedule::whereParameter(input('parameter'))->first();
        $grade    = \Startschool\Grade\Models\Grade::find($schedule->grade_id);
        $studentId= \Startschool\Grade\Models\Student::whereGradeId($schedule->grade_id)->pluck('student_id');
        $students = \Startschool\Student\Models\User::orderBy('name', 'asc')->whereIn('id', $studentId)->get();

        $spreadsheet = new Spreadsheet();
        $sheet       = $spreadsheet->getActiveSheet();

        foreach ($students as $key => $student) {
            $cell         = $key + 1;
            $isAttendance = \Startschool\Learn\Models\Student::whereScheduleId($schedule->id)->whereStudentId($student->id)->first();
            $sheet->setCellValue('A'.$cell, strtoupper($student->name));
            $sheet->setCellValue('B'.$cell, $grade->name);

            if($isAttendance) {
                $sheet->setCellValue('C'.$cell, $isAttendance->created_at->format('H:i'));
            }
            else {
                $sheet->setCellValue('C'.$cell, 'Belum Hadir');
            }
        }

        $generator = new \Startschool\Core\Classes\Generator;
        $writer    = new Xlsx($spreadsheet);
        $fileName  = $generator->make().'.xlsx';
        $path      = storage_path('app/media/attendance/'.$fileName);
        $writer->save($path);

        return response()->json([
            'result'   => true,
            'path'     => env('APP_URL').'storage/app/media/attendance/'.$fileName
        ]);
    }

    public function scheduleSave($userId)
    {
        $rules = [
            'started_at'  => 'required|date',
            'ended_at'    => 'required|date',
            'grade_id'    => 'required',
            'course_id'   => 'required',
            'name'        => 'required',
            'code'        => 'required',
            'description' => 'required',
        ];
        $attributeNames = [
            'started_at'  => 'tanggal mulai',
            'ended_at'    => 'sampai dengan',
            'grade_id'    => 'kelas',
            'course_id'   => 'pelajaran',
            'name'        => 'materi',
            'code'        => 'link video',
            'description' => 'deskripsi',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user    = \Startschool\User\Models\User::find($userId);
        $teacher = $user->teacher;


        $grades = post('grade_id');
        if(count($grades)) {
            foreach ($grades as $key => $grade) {
                $schedule = \Startschool\Learn\Models\Schedule::firstOrCreate([
                    'id' => post('id')
                ]);
                $schedule->teacher_id  = $teacher->id;
                $schedule->started_at  = Carbon::parse(post('started_at'))->format('Y-m-d H:i:s');
                $schedule->ended_at    = Carbon::parse(post('ended_at'))->format('Y-m-d H:i:s');;
                $schedule->grade_id    = $grade;
                $schedule->course_id   = post('course_id');
                $schedule->name        = post('name');
                $schedule->code        = post('code');
                $schedule->description = post('description');
                $schedule->save();

                if(\Input::hasFile('documents')) {
                    $files = \Input::file('documents');
                    foreach ($files as $key => $f) {
                        $file            = new \System\Models\File;
                        $file->data      = $f;
                        $file->save();
                        $schedule->documents()->add($file);
                    }
                }
            }
        }

        return response()->json([
            'result' => true
        ]);
    }

    public function scheduleDelete()
    {
        $schedule = \Startschool\Learn\Models\Schedule::whereParameter(input('parameter'))->first();

        if($schedule->documents) {
            $schedule->documents()->delete();
        }
        $schedule->delete();

        return response()->json([
            'result' => true
        ]);
    }

    public function scheduleFileDelete()
    {
        $key      = input('key');
        $schedule = \Startschool\Learn\Models\Schedule::whereParameter(input('parameter'))->first();
        $schedule->documents[$key]->delete();
        return response()->json([
            'result' => true,
        ]);
    }
}

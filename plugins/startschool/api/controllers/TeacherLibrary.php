<?php namespace Startschool\Api\Controllers;

use \Carbon\Carbon;

use Startschool\Api\Classes\ApiController;

class TeacherLibrary extends ApiController
{
    public function book($userId)
    {
        $books = \Startschool\Library\Models\Book::get();
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithCollection($books, new \Startschool\Api\Transformers\LibraryBookTransformer)
        ]);
    }

    public function bookDetail()
    {
        $book = \Startschool\Library\Models\Book::whereParameter(input('parameter'))->first();
        return response()->json([
            'result' => true,
            'response' => $this->respondWithItem($book, new \Startschool\Api\Transformers\LibraryBookTransformer)
        ]);
    }

    public function bookSave($userId)
    {
        $rules = [
            'title'     => 'required',
            'path'      => 'required',
            'grade'     => 'required',
            'publisher' => 'required',
        ];
        $attributeNames = [
            'title'     => 'judul',
            'path'      => 'url',
            'grade'     => 'tingkat',
            'publisher' => 'penerbit',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        // if(!post('id')) {
        //     if(!\Input::hasFile('file')) {
        //         return response()->json([
        //             'message' => 'File buku wajib di upload'
        //         ]);
        //     }
        // }

        if($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $book = \Startschool\Library\Models\Book::firstOrCreate([
            'id' => post('id')
        ]);
        $book->title     = post('title');
        $book->path      = post('path');
        $book->grade     = post('grade');
        $book->publisher = post('publisher');
        $book->save();

        if(\Input::hasFile('file')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('file');
            $file->save();
            $book->document()->add($file);
        }

        return response()->json([
            'result' => true
        ]);
    }

    public function bookDelete()
    {
        $book = \Startschool\Library\Models\Book::whereParameter(input('parameter'))->first();
        $book->delete();

        return response()->json([
            'result' => true
        ]);
    }
}

<?php


Route::group([
    'prefix'     => 'api/v1',
    'namespace'  => 'Startschool\Api\Controllers',
    // 'middleware' => 'Startschool\Api\Middleware\Jwt'
], function() {

    // Version
    Route::get('/android', function(){

    });

    Route::group(['prefix' => 'auth'], function(){
        Route::post('student', 'Auth@student');
        Route::post('teacher', 'Auth@teacher');
        Route::post('update-password', 'Auth@password');
    });

    Route::group(['prefix' => 'master'], function(){
        Route::group(['prefix' => 'student'], function(){
            Route::get('/', 'MasterStudent@get');
            Route::get('/find', 'MasterStudent@find');
            Route::post('/reset', 'MasterStudent@reset');
        });

        Route::group(['prefix' => 'event'], function(){
            Route::get('/', 'MasterEvent@get');
            Route::get('/detail', 'MasterEvent@detail');
            Route::get('/download', 'MasterEvent@download');
        });

        Route::group(['prefix' => 'access'], function(){
            Route::get('/', 'Access@get');
        });
    });

    Route::group(['prefix' => 'grade'], function(){
        Route::get('/', 'Grade@get');
        Route::get('/select', 'Grade@select');
        Route::get('/{slug}', 'Grade@detail');
    });

    /**
     * Vote
    */
    Route::group(['prefix' => 'election'], function(){
        Route::get('/mine', 'Election@mine');
        Route::post('/vote', 'Election@vote');
    });

    /**
     * Teacher
    */
    Route::group(['prefix' => 'teacher'], function(){
        Route::get('/', 'Teacher@get');

        Route::group(['prefix' => '/{userId}'], function(){
            // Ruang Akses
            Route::group(['prefix' => '/access'], function(){
                Route::get('/', 'TeacherAccess@get');
                Route::post('/', 'TeacherAccess@save');
            });

            // Ruang Perpustakaan
            Route::group(['prefix' => '/library'], function(){
                Route::group(['prefix' => '/book'], function(){
                    Route::get('/', 'TeacherLibrary@book');
                    Route::post('/', 'TeacherLibrary@bookSave');
                    Route::get('/detail', 'TeacherLibrary@bookDetail');
                    Route::get('/delete', 'TeacherLibrary@bookDelete');
                });
            });

            // Ruang Belajar
            Route::group(['prefix' => '/learn'], function(){
                Route::group(['prefix' => '/schedule'], function(){
                    Route::get('/', 'TeacherLearn@get');
                    Route::post('/', 'TeacherLearn@scheduleSave');
                    Route::get('/detail', 'TeacherLearn@scheduleDetail');
                    Route::get('/attendance', 'TeacherLearn@scheduleAttendance');
                    Route::get('/attendance/download', 'TeacherLearn@scheduleAttendanceDownload');
                    Route::get('/delete', 'TeacherLearn@scheduleDelete');
                    Route::get('/file/delete', 'TeacherLearn@scheduleFileDelete');
                });
            });

            // Ruang Tugas
            Route::group(['prefix' => '/work'], function(){
                Route::get('/', 'TeacherWork@get');
                Route::post('/', 'TeacherWork@save');
                Route::get('/delete', 'TeacherWork@delete');
                Route::get('/detail', 'TeacherWork@detail');
                Route::get('/result', 'TeacherWork@result');
                Route::post('/result/save', 'TeacherWork@resultSave');
                Route::get('/result/detail', 'TeacherWork@resultDetail');
                Route::get('/result/download', 'TeacherWork@resultDownload');

                Route::group(['prefix' => '/essay'], function(){
                    Route::get('/', 'TeacherWorkEssay@get');
                    Route::post('/', 'TeacherWorkEssay@save');
                    Route::get('/detail', 'TeacherWorkEssay@detail');
                    Route::get('/delete', 'TeacherWorkEssay@delete');
                    Route::get('/item', 'TeacherWorkEssay@item');
                    Route::post('/item', 'TeacherWorkEssay@itemSave');
                });
            });

            // Ruang Ujian
            Route::group(['prefix' => '/exam'], function(){
                Route::get('results', 'TeacherExam@result');

                Route::get('schedules', 'TeacherExam@schedule');
                Route::get('schedule', 'TeacherExam@scheduleDetail');
                Route::get('schedule/active', 'TeacherExam@scheduleActive');
                Route::get('schedule/graph', 'TeacherExam@scheduleGraph');
                Route::get('schedule/add', 'TeacherExam@scheduleAdd');
                Route::post('schedule', 'TeacherExam@scheduleSave');
                Route::get('schedule/edit', 'TeacherExam@scheduleEdit');
                Route::post('schedule/delete', 'TeacherExam@scheduleDelete');
                Route::get('schedule/excel', 'TeacherExam@scheduleExcel');

                Route::get('exams', 'TeacherExam@exam');
                Route::get('exam', 'TeacherExam@examDetail');
                Route::get('exam/edit', 'TeacherExam@examEdit');
                Route::post('exam/edit', 'TeacherExam@examEditSave');
                Route::post('exam/delete', 'TeacherExam@examDelete');
                Route::get('exam/import', 'TeacherExam@examImport');
                Route::get('exam/map', 'TeacherExam@examMap');
                Route::post('exam/map', 'TeacherExam@examMapSave');
                Route::delete('exam/map', 'TeacherExam@examMapDelete');
                Route::post('exam/import', 'TeacherExam@examImportSave');
                Route::get('exam/question', 'TeacherExam@examQuestionDetail');
                Route::post('exam/question', 'TeacherExam@examQuestionSave');
                Route::post('exam/question/image', 'TeacherExam@examQuestionImageSave');
            });

            // Ruang Akun
            Route::group(['prefix' => '/account'], function(){
                Route::group(['prefix' => '/event'], function(){
                    Route::get('/', 'TeacherAccount@eventGet');
                    Route::get('/detail', 'TeacherAccount@eventDetail');
                    Route::post('/', 'TeacherAccount@eventSave');
                });
            });
        });
    });

    /**
     * Student
     */
    Route::group(['prefix' => 'student'], function(){
        Route::get('/', 'Student@get');

        Route::group(['prefix' => '{userId}'], function(){

            // Ruang Belajar
            Route::group(['prefix' => 'learn'], function(){
                Route::group(['prefix' => 'schedule'], function(){
                    Route::get('/', 'StudentLearn@schedule');
                    Route::get('/detail', 'StudentLearn@scheduleDetail');
                    Route::post('/async', 'StudentLearn@async');
                    Route::post('/attendance', 'StudentLearn@attendance');
                });
            });

            // Ruang Tugas
            Route::group(['prefix' => 'work'], function(){
                Route::get('/', 'StudentWork@get');
                Route::get('/detail', 'StudentWork@detail');
                Route::get('/result', 'StudentWork@result');
                Route::get('/submit', 'StudentWork@submit');
                Route::post('/submit', 'StudentWork@submitSave');
            });

            // Ruang Ujian
            Route::group(['prefix' => 'exam'], function(){
                Route::get('/', 'StudentExam@exam');
                Route::get('/detail', 'StudentExam@examDetail');
                Route::get('/question', 'StudentExam@examQuestion');
                Route::post('/start', 'StudentExam@examStart');
                Route::post('/answer', 'StudentExam@examAnswer');
                Route::post('/massive', 'StudentExam@examMassive');
                Route::post('/end', 'StudentExam@examEnd');
            });

            // Ruang Perpustakaan
            Route::group(['prefix' => '/library'], function(){
                Route::group(['prefix' => '/book'], function(){
                    Route::get('/', 'StudentLibrary@book');
                });
            });
        });
    });
});

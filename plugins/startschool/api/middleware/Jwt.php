<?php namespace Startschool\Api\Middleware;

use Firebase\JWT\JWT as JWTToken;
use Closure;

class Jwt
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key     = \Request::header('Authorization');
        // $key     = base64_decode(strtr($key, '-_', '+/'));
        // $payload = \Startschool\Grade\Models\Grade::first();

        try {
            $decoded = JWTToken::decode($key, env('JWT_KEY'), array('HS256'));
            return $next($request);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}

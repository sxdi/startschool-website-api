<?php namespace Startschool\Library;

use Backend;
use System\Classes\PluginBase;

/**
 * Library Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Library',
            'description' => 'No description provided yet...',
            'author'      => 'Startschool',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Startschool\Library\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'startschool.library.some_permission' => [
                'tab' => 'Library',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'library' => [
                'label'       => 'Library',
                'url'         => Backend::url('startschool/library/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['startschool.library.*'],
                'order'       => 500,
            ],
        ];
    }
}

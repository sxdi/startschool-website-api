<?php namespace Startschool\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAccessCoursesTable extends Migration
{
    public function up()
    {
        Schema::create('startschool_user_access_courses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('course_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('startschool_user_access_courses');
    }
}

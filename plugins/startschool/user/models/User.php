<?php namespace Startschool\User\Models;

use Model;

/**
 * User Model
 */
class User extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'startschool_user_users';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['id', 'code'];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'student' => [
            'Startschool\Student\Models\User',
            'key'      => 'id',
            'otherKey' => 'user_id'
        ],
        'teacher' => [
            'Startschool\Teacher\Models\User',
            'key'      => 'id',
            'otherKey' => 'user_id'
        ]
    ];
    public $belongsToMany = [
        'accessGrades' => [
            'Startschool\Grade\Models\Grade',
            'table'    => 'startschool_user_access_grades',
            'key'      => 'user_id',
            'otherKey' => 'grade_id'
        ],
        'accessCourses' => [
            'Startschool\Course\Models\Course',
            'table'    => 'startschool_user_access_courses',
            'key'      => 'user_id',
            'otherKey' => 'course_id'
        ],
    ];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function getMyCourseId()
    {
        $teacher = $this->teacher;
        $courses = \Startschool\Grade\Models\Course::whereTeacherId($teacher->id)->pluck('course_id');
        // $courses = \Startschool\User\Models\AccessCourse::whereUserId($this->id)->get()->pluck('course_id');
        return $courses;
    }

    public function getMyGradeId()
    {
        $teacher = $this->teacher;
        $grades  = \Startschool\Grade\Models\Course::whereTeacherId($teacher->id)->pluck('grade_id');
        // $grades = \Startschool\User\Models\AccessGrade::whereUserId($this->id)->get()->pluck('grade_id');
        return $grades;
    }
}

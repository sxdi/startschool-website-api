<?php namespace Startschool\Course;

use Backend;
use System\Classes\PluginBase;

/**
 * Course Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Course',
            'description' => 'No description provided yet...',
            'author'      => 'Startschool',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Startschool\Course\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'startschool.course.some_permission' => [
                'tab' => 'Course',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'course' => [
                'label'       => 'Course',
                'url'         => Backend::url('startschool/course/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['startschool.course.*'],
                'order'       => 500,
            ],
        ];
    }
}

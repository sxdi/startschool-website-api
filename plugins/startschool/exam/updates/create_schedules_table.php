<?php namespace Startschool\Exam\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSchedulesTable extends Migration
{
    public function up()
    {
        Schema::create('startschool_exam_schedules', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('exam_id');
            $table->datetime('start');
            $table->datetime('end');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('startschool_exam_schedules');
    }
}

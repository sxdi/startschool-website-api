<?php namespace Startschool\Exam\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateExamsTable extends Migration
{
    public function up()
    {
        Schema::create('startschool_exam_exams', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('type_id');
            $table->string('name');
            $table->text('description');
            $table->datetime('start');
            $table->datetime('end');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('startschool_exam_exams');
    }
}

<?php namespace Startschool\Exam\Models;

use Model;

/**
 * Schedule Model
 */
class Schedule extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'startschool_exam_schedules';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'id',
        'exam_id',
        'type_id',
        'grade_id',
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'start',
        'end'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'exam' => [
            'Startschool\Exam\Models\Exam',
            'key'      => 'id',
            'otherKey' => 'exam_id'
        ],
        'type' => [
            'Startschool\Exam\Models\Type',
            'key'      => 'id',
            'otherKey' => 'type_id'
        ],
        'grade' => [
            'Startschool\Grade\Models\Grade',
            'key'      => 'id',
            'otherKey' => 'grade_id'
        ]
    ];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new \Startschool\Core\Classes\Generator;
        $this->parameter = $generator->make();
    }
}

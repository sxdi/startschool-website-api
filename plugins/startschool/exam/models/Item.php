<?php namespace Startschool\Exam\Models;

use Model;

/**
 * Item Model
 */
class Item extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'startschool_exam_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'exam_id',
        'question',
        'optionA',
        'optionB',
        'optionC',
        'optionD',
        'optionE',
        'answer'
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'exam' => [
            'Startschool\Exam\Models\Exam',
            'key'      => 'exam_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [
        'picture' => ['System\Models\File'],
        'optionA' => ['System\Models\File'],
        'optionB' => ['System\Models\File'],
        'optionC' => ['System\Models\File'],
        'optionD' => ['System\Models\File'],
        'optionE' => ['System\Models\File'],
    ];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new \Startschool\Core\Classes\Generator;
        $this->parameter = $generator->make();
    }
}

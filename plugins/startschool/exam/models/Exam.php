<?php namespace Startschool\Exam\Models;

use Model;

/**
 * Exam Model
 */
class Exam extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'startschool_exam_exams';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [
        'items' => [
            'Startschool\Exam\Models\Item',
            'key'      => 'exam_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsTo     = [
        'type' => [
            'Startschool\Exam\Models\Type',
            'key'      => 'type_id',
            'otherKey' => 'id'
        ],
        'course' => [
            'Startschool\Course\Models\Course',
            'key'     => 'course_id',
            'otherKey'=> 'id'
        ],
        'schedule' => [
            'Startschool\Exam\Models\Schedule',
            'key'      => 'id',
            'otherKey' => 'exam_id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [
        'document' => ['System\Models\File']
    ];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new \Startschool\Core\Classes\Generator;
        $this->parameter = $generator->make();
    }
}

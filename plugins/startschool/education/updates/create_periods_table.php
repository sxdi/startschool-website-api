<?php namespace Startschool\Education\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePeriodsTable extends Migration
{
    public function up()
    {
        Schema::create('startschool_education_periods', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('startschool_education_periods');
    }
}

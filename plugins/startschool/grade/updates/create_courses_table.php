<?php namespace Startschool\Grade\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCoursesTable extends Migration
{
    public function up()
    {
        Schema::create('startschool_grade_courses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('grade_id');
            $table->integer('teacher_id');
            $table->integer('course_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('startschool_grade_courses');
    }
}

<?php namespace Startschool\Grade\Models;

use Model;

/**
 * Grade Model
 */
class Grade extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'startschool_grade_grades';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [
        'students' => [
            'Startschool\Student\Models\User',
            'table'    => 'startschool_grade_students',
            'key'      => 'grade_id',
            'otherKey' => 'student_id'
        ]
    ];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function getStudentId()
    {
        $students = \Startschool\Grade\Models\Student::whereGradeId($this->id)->get()->pluck('id');
        return $students;
    }

    public function countStudent()
    {
        $students = \Startschool\Grade\Models\Student::whereGradeId($this->id)->count();
        return $students;
    }

    public function countMaleStudent()
    {
        $gradeId  = $this->id;
        $students = \Startschool\Grade\Models\Student::whereGradeId($this->id)->whereHas('user', function($q){
            $q->whereGender('male');
        })->count();
        return $students;
    }

    public function countFemaleStudent()
    {
        $gradeId  = $this->id;
        $students = \Startschool\Grade\Models\Student::whereGradeId($this->id)->whereHas('user', function($q){
            $q->whereGender('female');
        })->count();
        return $students;
    }
}

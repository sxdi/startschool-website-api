<?php namespace Startschool\Core;

use Backend;
use System\Classes\PluginBase;

/**
 * Core Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Core',
            'description' => 'No description provided yet...',
            'author'      => 'Startschool',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsoleCommand('startschool.init', 'Startschool\Core\Console\Initialize');
        $this->registerConsoleCommand('startschool.exam-print', 'Startschool\Core\Console\ExamPrint');
        $this->registerConsoleCommand('startschool.exam-start', 'Startschool\Core\Console\ExamStart');
        $this->registerConsoleCommand('startschool.exam-end', 'Startschool\Core\Console\ExamEnd');
        $this->registerConsoleCommand('startschool.force-learn', 'Startschool\Core\Console\ForceLearn');
        $this->registerConsoleCommand('startschool.force-work', 'Startschool\Core\Console\ForceWork');
        $this->registerConsoleCommand('startschool.force-event', 'Startschool\Core\Console\ForceEvent');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Startschool\Core\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'startschool.core.some_permission' => [
                'tab' => 'Core',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'core' => [
                'label'       => 'Core',
                'url'         => Backend::url('startschool/core/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['startschool.core.*'],
                'order'       => 500,
            ],
        ];
    }
}

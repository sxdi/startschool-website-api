<?php namespace Startschool\Core\Classes;

class Generator
{
    public function make()
    {
        return md5(\Hash::make('Y-m-d').$this->randNumber());
    }

    public function randNumber()
    {
        return rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
    }
}

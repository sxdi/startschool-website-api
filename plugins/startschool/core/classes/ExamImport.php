<?php namespace Startschool\Core\Classes;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ExamImport implements ToCollection
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $key => $row) {
            if($key != 0) {
                \Startschool\Exam\Models\Temporary::create([
                    'question' => $row[0],
                    'option_a' => $row[1],
                    'option_b' => $row[2],
                    'option_c' => $row[3],
                    'option_d' => $row[4],
                    'option_e' => $row[5],
                    'answer'   => $row[6]
                ]);
            }
        }
    }
}

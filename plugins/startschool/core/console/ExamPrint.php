<?php namespace Startschool\Core\Console;

use Carbon\Carbon;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ExamPrint extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'startschool:exam-print';

    /**
     * @var string The console command description.
     */
    protected $description = 'Does something cool.';


    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $data    = [
            // [
            //     'name'  => '2020-09-29 07:30:00',
            //     'table' => 'startschool_exam_students_bak29sep1',
            //     'path'  => '29-1'
            // ],[
            //     'name'  => '2020-09-29 10:00:00',
            //     'table' => 'startschool_exam_students_bak29sep2',
            //     'path'  => '29-2'
            // ],[
            //     'name'  => '2020-09-29 13:00:00',
            //     'table' => 'startschool_exam_students_bak29sep3',
            //     'path'  => '29-3'
            // ],
            [
                'name'  => '2020-09-25 15:30:00',
                'table' => 'startschool_exam_students_bak25sep4',
                'path'  => '25-4'
            ],
        ];

        foreach ($data as $key => $dat) {
            $this->info('Processing data for date '.$dat['name']);
            $schedules= \Startschool\Exam\Models\Schedule::where('start', 'like', '%'.$dat['name'].'%')->orderBy('grade_id')->get();
            foreach ($schedules as $key => $schedule) {
                $students = \Startschool\Student\Models\User::whereHas('grade', function($q) use($schedule){
                    $q->whereGradeId($schedule->grade_id);
                })->orderBy('name', 'asc')->get();

                $results     = [];
                $reader      = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
                $spreadsheet = $reader;
                $reportSheet = $spreadsheet->setActiveSheetIndex(0);

                $styleHeader = [
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'right' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'left' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'rgb' => '8eb4cb',
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical'    => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                    ]
                ];
                $styleDate = [
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'right' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'left' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical'    => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                    ]
                ];
                $styleBody = [
                    'borders' => [
                        'left' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'right' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ];
                $styleBodyLast = [
                    'borders' => [
                        'right' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ];

                $spreadsheet->getActiveSheet()->mergeCells('B2:B3');
                $spreadsheet->getActiveSheet()->getStyle('B2:B3')->applyFromArray($styleHeader);
                $reportSheet->getCell('B2')->setValue('NAMA');
                $spreadsheet->getActiveSheet()->mergeCells('C2:C3');
                $spreadsheet->getActiveSheet()->getStyle('C2:C3')->applyFromArray($styleHeader);
                $reportSheet->getCell('C2')->setValue('NIS');
                $spreadsheet->getActiveSheet()->mergeCells('D2:D3');
                $spreadsheet->getActiveSheet()->getStyle('D2:D3')->applyFromArray($styleHeader);
                $reportSheet->getCell('D2')->setValue('NISN');
                $spreadsheet->getActiveSheet()->mergeCells('E2:I2');
                $spreadsheet->getActiveSheet()->getStyle('E2:I2')->applyFromArray($styleHeader);
                $reportSheet->getCell('E2')->setValue('HASIL');
                $spreadsheet->getActiveSheet()->getStyle('E3')->applyFromArray($styleHeader);
                $reportSheet->getCell('E3')->setValue('SOAL');
                $spreadsheet->getActiveSheet()->getStyle('F3')->applyFromArray($styleHeader);
                $reportSheet->getCell('F3')->setValue('TERJAWAB');
                $spreadsheet->getActiveSheet()->getStyle('G3')->applyFromArray($styleHeader);
                $reportSheet->getCell('G3')->setValue('SALAH');
                $spreadsheet->getActiveSheet()->getStyle('H3')->applyFromArray($styleHeader);
                $reportSheet->getCell('H3')->setValue('BENAR');
                $spreadsheet->getActiveSheet()->getStyle('I3')->applyFromArray($styleHeader);
                $reportSheet->getCell('I3')->setValue('NILAI');

                $cell = 4;
                foreach ($students as $key => $student) {
                    $key = $key + 1;
                    $code= $student->nisn;
                    $url = "sheet://'$code'!B3";

                    // put name
                    $spreadsheet->getActiveSheet()->getStyle('B'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('B'.$cell)->setValue(strtoupper($student->name))->getHyperlink()->setUrl($url);

                    $spreadsheet->getActiveSheet()->getStyle('C'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('C'.$cell)->setValue($student->nis);

                    $spreadsheet->getActiveSheet()->getStyle('D'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('D'.$cell)->setValue($student->nisn);

                    $totalWrong = [];
                    $totalTrue  = [];
                    $totalAsnwer= [];

                    $worksheet1 = $spreadsheet->createSheet();
                    $worksheet1->setTitle((string)$student->nisn);
                    $studentSheet = $spreadsheet->setActiveSheetIndex($key);

                    $spreadsheet->getActiveSheet()->getStyle('B2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('B2')->setValue('NOMOR');
                    $spreadsheet->getActiveSheet()->getStyle('C2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('C2')->setValue('PERTANYAAN');
                    $spreadsheet->getActiveSheet()->getStyle('D2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('D2')->setValue('PILIHAN A');
                    $spreadsheet->getActiveSheet()->getStyle('E2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('E2')->setValue('PILIHAN B');
                    $spreadsheet->getActiveSheet()->getStyle('F2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('F2')->setValue('PILIHAN C');
                    $spreadsheet->getActiveSheet()->getStyle('G2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('G2')->setValue('PILIHAN D');
                    $spreadsheet->getActiveSheet()->getStyle('H2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('H2')->setValue('PILIHAN E');
                    $spreadsheet->getActiveSheet()->getStyle('I2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('I2')->setValue('KUNCI');
                    $spreadsheet->getActiveSheet()->getStyle('J2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('J2')->setValue('JAWABAN');
                    $spreadsheet->getActiveSheet()->getStyle('K2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('K2')->setValue('HASIL');
                    $cellResume = 3;

                    foreach ($schedule->exam->items as $keyItem => $item) {
                        // $isExist = \Startschool\Exam\Models\Student::whereStudentId($student->id)->whereItemId($item->id)->whereExamId($schedule->exam_id)->first();
                        $isExist    = \DB::table($dat['table'])->where('student_id', $student->id)->where('item_id', $item->id)->where('exam_id', $schedule->exam_id)->first();
                        // $studentSheet->getCell('B'.$cellResume)->setValue((string)$keyItem+1);
                        $spreadsheet->getActiveSheet()->getStyle('B'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('C'.$cellResume)->setValue($item->question);
                        $spreadsheet->getActiveSheet()->getStyle('C'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('D'.$cellResume)->setValue($item->option_a);
                        $spreadsheet->getActiveSheet()->getStyle('D'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('E'.$cellResume)->setValue($item->option_b);
                        $spreadsheet->getActiveSheet()->getStyle('E'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('F'.$cellResume)->setValue($item->option_c);
                        $spreadsheet->getActiveSheet()->getStyle('F'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('G'.$cellResume)->setValue($item->option_d);
                        $spreadsheet->getActiveSheet()->getStyle('G'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('H'.$cellResume)->setValue($item->option_e);
                        $spreadsheet->getActiveSheet()->getStyle('H'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('I'.$cellResume)->setValue($item->answer);
                        $spreadsheet->getActiveSheet()->getStyle('I'.$cellResume)->applyFromArray($styleBody);


                        if($isExist) {
                            $studentSheet->getCell('J'.$cellResume)->setValue($isExist->answer);
                            $spreadsheet->getActiveSheet()->getStyle('J'.$cellResume)->applyFromArray($styleBody);
                            array_push($totalAsnwer, '1');

                            if($isExist->key != $isExist->answer) {
                                $studentSheet->getCell('K'.$cellResume)->setValue('SALAH');
                                $spreadsheet->getActiveSheet()->getStyle('K'.$cellResume)->applyFromArray($styleBody);
                                array_push($totalWrong, '1');
                            }
                            else {
                                $studentSheet->getCell('K'.$cellResume)->setValue('BENAR');
                                $spreadsheet->getActiveSheet()->getStyle('K'.$cellResume)->applyFromArray($styleBody);
                                array_push($totalTrue, '1');
                            }
                        }
                        else {
                            $studentSheet->getCell('J'.$cellResume)->setValue('Tidak terjawab');
                            $spreadsheet->getActiveSheet()->getStyle('J'.$cellResume)->applyFromArray($styleBody);
                        }

                        $cellResume++;
                    }

                    $reportSheet = $spreadsheet->setActiveSheetIndex(0);
                    $spreadsheet->getActiveSheet()->getStyle('E'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('E'.$cell)->setValue((string) $schedule->exam->items->count());
                    $spreadsheet->getActiveSheet()->getStyle('F'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('F'.$cell)->setValue((string) count($totalAsnwer));
                    $spreadsheet->getActiveSheet()->getStyle('G'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('G'.$cell)->setValue((string) count($totalWrong));
                    $spreadsheet->getActiveSheet()->getStyle('H'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('H'.$cell)->setValue((string) count($totalTrue));
                    $spreadsheet->getActiveSheet()->getStyle('I'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('I'.$cell)->setValue((string) ceil(count($totalTrue)/$schedule->exam->items->count()*100));
                    $cell++;
                }

                $generator = new \Startschool\Core\Classes\Generator;
                $filename  = '['.str_slug($schedule->grade->name).'] - '.$generator->make().'.xls';
                $tmp       = storage_path('app/media/'.$dat['path'].'/'.$filename);
                $writer    = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
                $writer->save($tmp);
                $this->info('Success Export '.$schedule->grade->name);
                // return response()->json([
                //     'result'    => true,
                //     'response'  => env('APP_URL').'storage/app/media/'.$filename
                // ]);
            }
        }
    }


    /**
     *
     */
    public function handleToday()
    {
        $data    = [
            '07:30:00',
            '10:00:00',
            '13:00:00',
            '15:30:00'
        ];
        $now     = date('Y-m-d');

        foreach ($data as $key => $dat) {
            $dateNow = $now.' '.$dat;
            $this->info('Processing data for date '.$dateNow);
            $schedules= \Startschool\Exam\Models\Schedule::where('start', 'like', '%'.$dateNow.'%')->orderBy('grade_id')->get();
            foreach ($schedules as $keySchedule => $schedule) {
                $students = \Startschool\Student\Models\User::whereHas('grade', function($q) use($schedule){
                    $q->whereGradeId($schedule->grade_id);
                })->orderBy('name', 'asc')->get();

                $results     = [];
                $reader      = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
                $spreadsheet = $reader;
                $reportSheet = $spreadsheet->setActiveSheetIndex(0);

                $styleHeader = [
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'right' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'left' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'rgb' => '8eb4cb',
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical'    => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                    ]
                ];
                $styleDate = [
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'right' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'left' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical'    => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                    ]
                ];
                $styleBody = [
                    'borders' => [
                        'left' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'right' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ];
                $styleBodyLast = [
                    'borders' => [
                        'right' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ];

                $spreadsheet->getActiveSheet()->mergeCells('B2:B3');
                $spreadsheet->getActiveSheet()->getStyle('B2:B3')->applyFromArray($styleHeader);
                $reportSheet->getCell('B2')->setValue('NAMA');
                $spreadsheet->getActiveSheet()->mergeCells('C2:C3');
                $spreadsheet->getActiveSheet()->getStyle('C2:C3')->applyFromArray($styleHeader);
                $reportSheet->getCell('C2')->setValue('NIS');
                $spreadsheet->getActiveSheet()->mergeCells('D2:D3');
                $spreadsheet->getActiveSheet()->getStyle('D2:D3')->applyFromArray($styleHeader);
                $reportSheet->getCell('D2')->setValue('NISN');
                $spreadsheet->getActiveSheet()->mergeCells('E2:I2');
                $spreadsheet->getActiveSheet()->getStyle('E2:I2')->applyFromArray($styleHeader);
                $reportSheet->getCell('E2')->setValue('HASIL');
                $spreadsheet->getActiveSheet()->getStyle('E3')->applyFromArray($styleHeader);
                $reportSheet->getCell('E3')->setValue('SOAL');
                $spreadsheet->getActiveSheet()->getStyle('F3')->applyFromArray($styleHeader);
                $reportSheet->getCell('F3')->setValue('TERJAWAB');
                $spreadsheet->getActiveSheet()->getStyle('G3')->applyFromArray($styleHeader);
                $reportSheet->getCell('G3')->setValue('SALAH');
                $spreadsheet->getActiveSheet()->getStyle('H3')->applyFromArray($styleHeader);
                $reportSheet->getCell('H3')->setValue('BENAR');
                $spreadsheet->getActiveSheet()->getStyle('I3')->applyFromArray($styleHeader);
                $reportSheet->getCell('I3')->setValue('NILAI');

                $cell = 4;
                foreach ($students as $key => $student) {
                    $key = $key + 1;
                    $code= $student->nisn;
                    $url = "sheet://'$code'!B3";

                    // put name
                    $spreadsheet->getActiveSheet()->getStyle('B'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('B'.$cell)->setValue(strtoupper($student->name))->getHyperlink()->setUrl($url);

                    $spreadsheet->getActiveSheet()->getStyle('C'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('C'.$cell)->setValue($student->nis);

                    $spreadsheet->getActiveSheet()->getStyle('D'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('D'.$cell)->setValue($student->nisn);

                    $totalWrong = [];
                    $totalTrue  = [];
                    $totalAsnwer= [];

                    $worksheet1 = $spreadsheet->createSheet();
                    $worksheet1->setTitle((string)$student->nisn);
                    $studentSheet = $spreadsheet->setActiveSheetIndex($key);

                    $spreadsheet->getActiveSheet()->getStyle('B2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('B2')->setValue('NOMOR');
                    $spreadsheet->getActiveSheet()->getStyle('C2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('C2')->setValue('PERTANYAAN');
                    $spreadsheet->getActiveSheet()->getStyle('D2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('D2')->setValue('PILIHAN A');
                    $spreadsheet->getActiveSheet()->getStyle('E2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('E2')->setValue('PILIHAN B');
                    $spreadsheet->getActiveSheet()->getStyle('F2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('F2')->setValue('PILIHAN C');
                    $spreadsheet->getActiveSheet()->getStyle('G2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('G2')->setValue('PILIHAN D');
                    $spreadsheet->getActiveSheet()->getStyle('H2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('H2')->setValue('PILIHAN E');
                    $spreadsheet->getActiveSheet()->getStyle('I2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('I2')->setValue('KUNCI');
                    $spreadsheet->getActiveSheet()->getStyle('J2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('J2')->setValue('JAWABAN');
                    $spreadsheet->getActiveSheet()->getStyle('K2')->applyFromArray($styleHeader);
                    $studentSheet->getCell('K2')->setValue('HASIL');
                    $cellResume = 3;

                    foreach ($schedule->exam->items as $keyItem => $item) {
                        // $isExist = \Startschool\Exam\Models\Student::whereStudentId($student->id)->whereItemId($item->id)->whereExamId($schedule->exam_id)->first();
                        $table      = 'startschool_exam_students_bak'.date('d').'sep'.($keySchedule+1);
                        $isExist    = \DB::table($table)->where('student_id', $student->id)->where('item_id', $item->id)->where('exam_id', $schedule->exam_id)->first();
                        // $studentSheet->getCell('B'.$cellResume)->setValue((string)$keyItem+1);
                        $spreadsheet->getActiveSheet()->getStyle('B'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('C'.$cellResume)->setValue($item->question);
                        $spreadsheet->getActiveSheet()->getStyle('C'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('D'.$cellResume)->setValue($item->option_a);
                        $spreadsheet->getActiveSheet()->getStyle('D'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('E'.$cellResume)->setValue($item->option_b);
                        $spreadsheet->getActiveSheet()->getStyle('E'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('F'.$cellResume)->setValue($item->option_c);
                        $spreadsheet->getActiveSheet()->getStyle('F'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('G'.$cellResume)->setValue($item->option_d);
                        $spreadsheet->getActiveSheet()->getStyle('G'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('H'.$cellResume)->setValue($item->option_e);
                        $spreadsheet->getActiveSheet()->getStyle('H'.$cellResume)->applyFromArray($styleBody);
                        $studentSheet->getCell('I'.$cellResume)->setValue($item->answer);
                        $spreadsheet->getActiveSheet()->getStyle('I'.$cellResume)->applyFromArray($styleBody);


                        if($isExist) {
                            $studentSheet->getCell('J'.$cellResume)->setValue($isExist->answer);
                            $spreadsheet->getActiveSheet()->getStyle('J'.$cellResume)->applyFromArray($styleBody);
                            array_push($totalAsnwer, '1');

                            if($isExist->key != $isExist->answer) {
                                $studentSheet->getCell('K'.$cellResume)->setValue('SALAH');
                                $spreadsheet->getActiveSheet()->getStyle('K'.$cellResume)->applyFromArray($styleBody);
                                array_push($totalWrong, '1');
                            }
                            else {
                                $studentSheet->getCell('K'.$cellResume)->setValue('BENAR');
                                $spreadsheet->getActiveSheet()->getStyle('K'.$cellResume)->applyFromArray($styleBody);
                                array_push($totalTrue, '1');
                            }
                        }
                        else {
                            $studentSheet->getCell('J'.$cellResume)->setValue('Tidak terjawab');
                            $spreadsheet->getActiveSheet()->getStyle('J'.$cellResume)->applyFromArray($styleBody);
                        }

                        $cellResume++;
                    }

                    $reportSheet = $spreadsheet->setActiveSheetIndex(0);
                    $spreadsheet->getActiveSheet()->getStyle('E'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('E'.$cell)->setValue((string) $schedule->exam->items->count());
                    $spreadsheet->getActiveSheet()->getStyle('F'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('F'.$cell)->setValue((string) count($totalAsnwer));
                    $spreadsheet->getActiveSheet()->getStyle('G'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('G'.$cell)->setValue((string) count($totalWrong));
                    $spreadsheet->getActiveSheet()->getStyle('H'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('H'.$cell)->setValue((string) count($totalTrue));
                    $spreadsheet->getActiveSheet()->getStyle('I'.$cell)->applyFromArray($styleBody);
                    $reportSheet->getCell('I'.$cell)->setValue((string) ceil(count($totalTrue)/$schedule->exam->items->count()*100));
                    $cell++;
                }

                $generator = new \Startschool\Core\Classes\Generator;
                $pattern   = date('d').'-'.($keySchedule+1);
                $filename  = '['.str_slug($schedule->grade->name).'] - '.$generator->make().'.xls';
                $tmp       = storage_path('app/media/today/'.$pattern.'/'.$filename);
                $writer    = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
                $writer->save($tmp);
                $this->info('Success Export '.$schedule->grade->name);
                // return response()->json([
                //     'result'    => true,
                //     'response'  => env('APP_URL').'storage/app/media/'.$filename
                // ]);
            }
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}

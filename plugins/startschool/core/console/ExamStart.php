<?php namespace Startschool\Core\Console;

use Carbon\Carbon;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ExamStart extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'startschool:exam-start';

    /**
     * @var string The console command description.
     */
    protected $description = 'Does something cool.';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $now   = date('Y-m-d H:i');
        $exams = \Startschool\Exam\Models\Schedule::whereIsActive(0)->whereIsEnd(0)->get();
        if(count($exams)) {
            foreach ($exams as $exam) {
                if($exam->start->format('Y-m-d H:i') == $now) {
                    $exam->is_active = 1;
                    $exam->save();
                }
            }
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}

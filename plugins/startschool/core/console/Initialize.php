<?php namespace Startschool\Core\Console;

use \Firebase\JWT\JWT;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Initialize extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'startschool:init';

    /**
     * @var string The console command description.
     */
    protected $description = 'Does something cool.';

    /**
     * Execute the console command.
     * @return void
    */
    public function handle()
    {
        $this->handleNewStudent();
    }

    /**
     * Handle Generator
     */
    public function handleGenerator()
    {
        $generator = new \Startschool\Core\Classes\Generator;
        $this->info($generator->make());
        return;
    }

    public function handleNewStudent()
    {
        $users = \DB::table('startschool_user_users_student_new')->get();
        foreach ($users as $key => $user) {
            // Create User
            $us = \Startschool\User\Models\User::firstOrNew([
                'code' => $user->nisn
            ]);
            $us->password = \Hash::make($user->nisn);
            $us->save();

            switch ($user->gender) {
                case 'L':
                    $gender = 'male';
                    break;

                case 'P':
                    $gender = 'female';
                    break;

                default:
                    $gender = 'female';
                    break;
            }

            // Create Student
            $student          = new \Startschool\Student\Models\User;
            $student->user_id = $us->id;
            $student->nis     = 'D';
            $student->nisn    = $user->nisn;
            $student->gender  = $gender;
            $student->save();

            // Create Grade
            $grade = \Startschool\Grade\Models\Grade::where('name', 'like', '%'.$user->grade.'%')->first();
            $studentGrade = \Startschool\Grade\Models\Student::firstOrNew([
                'grade_id' => $grade->id,
                'student_id' => $student->id
            ]);
            $studentGrade->save();

            $info = $key.' of '.count($users).' successfuly insert';
            $this->info($info);
        }
    }

    /**
     * Handle Course
     */
    public function handleCourse()
    {
        $courses = \DB::table('startschool_user_users_course')->get();
        foreach ($courses as $key => $course) {
            // Insert Grade
            $grade = \Startschool\Grade\Models\Grade::whereName($course->grade)->first();

            // Get Teacher
            // $teacherName = str_replace(' ', '', $course->name, 1);
            $teacherName = trim($course->name);
            $teacher = \Startschool\Teacher\Models\User::where('name', 'like', '%'.$teacherName.'%')->first();

            // Get Course
            $cour    = \Startschool\Course\Models\Course::where('name', 'like', '%'.$course->course.'%')->first();

            // Insert Grade Crouse
            $gradeCourse = \Startschool\Grade\Models\Course::firstOrNew([
                'grade_id'      => $grade->id,
                'teacher_id'    => $teacher->id,
                'course_id'     => $cour->id
            ]);
            $gradeCourse->save();

            $counter = 'inserting '.($key+1).' of '.count($courses);
            $this->info($counter);
        }
    }


    /**
     * Handle Teacher
    */
    public function handleTeacher()
    {
        $teachers = \DB::table('startschool_user_users_teacher_bak')->get();
        foreach ($teachers as $key => $teacher) {
            // Insert User
            $user = \Startschool\User\Models\User::firstOrNew([
                'code' => $teacher->nik
            ]);
            $user->password = \Hash::make($teacher->nik);
            $user->is_admin = $teacher->is_admin;
            $user->save();

            if($teacher->gender == 'P') {
                $gender = 'male';
            }
            else {
                $gender = 'female';
            }

            // Insert Teacher
            $tea = \Startschool\Teacher\Models\User::firstOrNew([
                'user_id' => $user->id
            ]);
            $tea->nik     = $teacher->nik;
            $tea->name    = $teacher->name;
            $tea->gender  = $gender;
            $tea->save();

            $counter = $key + 1;
            $this->info('inserting '.$counter.' of '.count($teachers));
        }
    }

    /**
     * Handle Student
     */
    public function handleStudent()
    {
        $students = \DB::table('startschool_user_users_student')->get();
        foreach ($students as $key => $student) {
            // Insert User
            $user = \Startschool\User\Models\User::firstOrNew([
                'id' => null
            ]);
            $user->code     = $student->nisn;
            $user->password = \Hash::make($student->nisn);
            $user->save();

            if($student->gender == 'P') {
                $gender = 'male';
            }
            else {
                $gender = 'female';
            }

            // Insert Student
            $stu = \Startschool\Student\Models\User::firstOrNew([
                'id' => null
            ]);
            $stu->user_id = $user->id;
            $stu->nis     = $student->nis;
            $stu->nisn    = $student->nisn;
            $stu->name    = $student->name;
            $stu->gender  = $gender;
            $stu->save();

            $counter = $key + 1;
            $this->info('inserting '.$key.' of '.count($students));
        }
    }

    public function handleGrade()
    {
        $grades = \DB::table('qqm')->get();
        foreach ($grades as $key => $grs) {
            $name    = $grs->grade;
            $student = \Startschool\Student\Models\User::where('nisn', 'like', '%'.$grs->nisn.'%')->first();
            $gr      = \Startschool\Grade\Models\Grade::where('name', 'like', '%'.$name.'%')->first();

            $m = \Startschool\Grade\Models\Student::firstOrNew([
                'grade_id'   => $gr ? $gr->id : 99,
                'student_id' => $student ? $student->id : 99
            ]);
            $m->save();

            $counter = $key + 1;
            $this->info('inserting '.$counter.' of '.count($grades));
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}

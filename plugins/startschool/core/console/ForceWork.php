<?php namespace Startschool\Core\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ForceWork extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'startschool:force-work';

    /**
     * @var string The console command description.
     */
    protected $description = 'Does something cool.';

    /**
     * Execute the console command.
     * @return void
    */
    public function handle()
    {
        $now     = \Carbon\Carbon::now()->format('Y-m-d');
        $learnIds= \Startschool\Work\Models\Work::where('started_at', 'like', '%'.$now.'%')->pluck('id');
        $learns  = \Startschool\Work\Models\Work::whereIn('id', $learnIds)->update([
            'status' => 'active'
        ]);

        // $now      = \Carbon\Carbon::parse($now)->addDays(1)->format('Y-m-d');
        $learnIds = [];
        $learnIds = \Startschool\Work\Models\Work::where('ended_at', 'like', '%'.$now.'%')->pluck('id');
        $learns   = \Startschool\Work\Models\Work::whereIn('id', $learnIds)->update([
            'status' => 'end'
        ]);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}

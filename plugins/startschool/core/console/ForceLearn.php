<?php namespace Startschool\Core\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ForceLearn extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'startschool:force-learn';

    /**
     * @var string The console command description.
     */
    protected $description = 'Does something cool.';

    /**
     * Execute the console command.
     * @return void
    */
    public function handle()
    {
        $now      = \Carbon\Carbon::now()->format('Y-m-d H:i');
        $learnIds = \Startschool\Learn\Models\Schedule::where('started_at', 'like', '%'.$now.'%')->pluck('id');
        $learnIds = \Startschool\Learn\Models\Schedule::whereIn('id', $learnIds)->update([
            'status' => 'active'
        ]);

        $learnIds = [];
        $learnIds = \Startschool\Learn\Models\Schedule::where('ended_at', 'like', '%'.$now.'%')->pluck('id');
        $learns   = \Startschool\Learn\Models\Schedule::whereIn('id', $learnIds)->update([
            'status' => 'end'
        ]);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}

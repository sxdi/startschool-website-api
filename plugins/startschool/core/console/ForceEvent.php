<?php namespace Startschool\Core\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ForceEvent extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'startschool:force-event';

    /**
     * @var string The console command description.
     */
    protected $description = 'Does something cool.';

    /**
     * Execute the console command.
     * @return void
    */
    public function handle()
    {
        $now   = \Carbon\Carbon::now()->format('Y-m-d');
        $event = \Startschool\Event\Models\Event::firstOrNew([
            'date' => $now
        ]);
        $event->save();
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}

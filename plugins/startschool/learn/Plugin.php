<?php namespace Startschool\Learn;

use Backend;
use System\Classes\PluginBase;

/**
 * Learn Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Learn',
            'description' => 'No description provided yet...',
            'author'      => 'Startschool',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Startschool\Learn\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'startschool.learn.some_permission' => [
                'tab' => 'Learn',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'learn' => [
                'label'       => 'Learn',
                'url'         => Backend::url('startschool/learn/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['startschool.learn.*'],
                'order'       => 500,
            ],
        ];
    }
}

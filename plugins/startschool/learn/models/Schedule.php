<?php namespace Startschool\Learn\Models;

use Model;

/**
 * Schedule Model
 */
class Schedule extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'startschool_learn_schedules';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'id'
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'started_at',
        'ended_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'teacher' => [
            'Startschool\Teacher\Models\User',
            'key'      => 'teacher_id',
            'otherKey' => 'id'
        ],
        'course' => [
            'Startschool\Course\Models\Course',
            'key'      => 'course_id',
            'otherKey' => 'id'
        ],
        'grade' => [
            'Startschool\Grade\Models\Grade',
            'key'      => 'grade_id',
            'otherKey' => 'id'
        ],
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [
        'documents' => ['System\Models\File']
    ];

    public function beforeCreate()
    {
        $generator = new \Startschool\Core\Classes\Generator;
        $this->parameter = $generator->make();
    }
}

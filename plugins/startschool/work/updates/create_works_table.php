<?php namespace Startschool\Work\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWorksTable extends Migration
{
    public function up()
    {
        Schema::create('startschool_work_works', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('grade_id');
            $table->string('name');
            $table->enum('type', ['file', 'essay']);
            $table->date('started_at');
            $table->date('ended_at');
            $table->timestamps();
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('startschool_work_works');
    }
}

<?php namespace Startschool\Work\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateResultsTable extends Migration
{
    public function up()
    {
        Schema::create('startschool_work_results', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('work_id');
            $table->integer('student_id');
            $table->timestamps();
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('startschool_work_results');
    }
}

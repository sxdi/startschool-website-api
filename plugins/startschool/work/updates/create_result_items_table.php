<?php namespace Startschool\Work\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateResultItemsTable extends Migration
{
    public function up()
    {
        Schema::create('startschool_work_result_items', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('work_id');
            $table->integer('essay_id');
            $table->text('question');
            $table->text('answer');
            $table->timestamps();
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('startschool_work_result_items');
    }
}

<?php namespace Startschool\Work\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateEssaysTable extends Migration
{
    public function up()
    {
        Schema::create('startschool_work_essays', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('teacher_id');
            $table->integer('course_id');
            $table->string('name');
            $table->timestamps();
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('startschool_work_essays');
    }
}

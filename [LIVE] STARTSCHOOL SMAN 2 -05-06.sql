# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.19)
# Database: startschool
# Generation Time: 2020-05-05 17:31:51 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table startschool_education_periods
# ------------------------------------------------------------

DROP TABLE IF EXISTS `startschool_education_periods`;

CREATE TABLE `startschool_education_periods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `startschool_education_periods` WRITE;
/*!40000 ALTER TABLE `startschool_education_periods` DISABLE KEYS */;

INSERT INTO `startschool_education_periods` (`id`, `name`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'Semester Genap Tahun Ajaran 2019 - 2020',1,'2020-05-05 20:51:46','2020-05-05 20:51:46');

/*!40000 ALTER TABLE `startschool_education_periods` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table startschool_grade_grades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `startschool_grade_grades`;

CREATE TABLE `startschool_grade_grades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `period_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `startschool_grade_grades` WRITE;
/*!40000 ALTER TABLE `startschool_grade_grades` DISABLE KEYS */;

INSERT INTO `startschool_grade_grades` (`id`, `period_id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,1,'X IPA 1','2020-05-05 13:56:28','2020-05-05 13:56:28'),
	(2,1,'X IPA 2','2020-05-05 13:56:28','2020-05-05 13:56:28'),
	(3,1,'X IPA 3','2020-05-05 13:56:28','2020-05-05 13:56:28'),
	(4,1,'X IPA 4','2020-05-05 13:56:28','2020-05-05 13:56:28'),
	(5,1,'X IPA 5','2020-05-05 13:56:28','2020-05-05 13:56:28'),
	(6,1,'X IPA 6','2020-05-05 13:56:28','2020-05-05 13:56:28'),
	(7,1,'X IPA 7','2020-05-05 13:56:28','2020-05-05 13:56:28'),
	(8,1,'X IPA 8','2020-05-05 13:56:28','2020-05-05 13:56:28'),
	(9,1,'X IPA 9','2020-05-05 13:56:28','2020-05-05 13:56:28'),
	(10,1,'X IPS 1','2020-05-05 13:56:41','2020-05-05 13:56:41'),
	(11,1,'X IPS 2','2020-05-05 13:56:41','2020-05-05 13:56:41'),
	(12,1,'X IPS 3','2020-05-05 13:56:41','2020-05-05 13:56:41'),
	(13,1,'X IPS 4','2020-05-05 13:56:41','2020-05-05 13:56:41'),
	(14,1,'X IPS 5','2020-05-05 13:56:41','2020-05-05 13:56:41'),
	(15,1,'X IPS 6','2020-05-05 13:56:41','2020-05-05 13:56:41'),
	(16,1,'XI IPA 1','2020-05-05 13:56:56','2020-05-05 13:56:56'),
	(17,1,'XI IPA 2','2020-05-05 13:56:56','2020-05-05 13:56:56'),
	(18,1,'XI IPA 3','2020-05-05 13:56:56','2020-05-05 13:56:56'),
	(19,1,'XI IPA 4','2020-05-05 13:56:56','2020-05-05 13:56:56'),
	(20,1,'XI IPA 5','2020-05-05 13:56:56','2020-05-05 13:56:56'),
	(21,1,'XI IPA 6','2020-05-05 13:56:56','2020-05-05 13:56:56'),
	(22,1,'XI IPA 7','2020-05-05 13:56:56','2020-05-05 13:56:56'),
	(23,1,'XI IPA 8','2020-05-05 13:56:56','2020-05-05 13:56:56'),
	(24,1,'XI IPA 9','2020-05-05 13:56:56','2020-05-05 13:56:56'),
	(25,1,'XI IPS 1','2020-05-05 13:57:01','2020-05-05 13:57:01'),
	(26,1,'XI IPS 2','2020-05-05 13:57:01','2020-05-05 13:57:01'),
	(27,1,'XI IPS 3','2020-05-05 13:57:01','2020-05-05 13:57:01'),
	(28,1,'XI IPS 4','2020-05-05 13:57:01','2020-05-05 13:57:01'),
	(29,1,'XI IPS 5','2020-05-05 13:57:01','2020-05-05 13:57:01'),
	(30,1,'XII IPA 1','2020-05-05 13:57:12','2020-05-05 13:57:12'),
	(31,1,'XII IPA 2','2020-05-05 13:57:12','2020-05-05 13:57:12'),
	(32,1,'XII IPA 3','2020-05-05 13:57:12','2020-05-05 13:57:12'),
	(33,1,'XII IPA 4','2020-05-05 13:57:12','2020-05-05 13:57:12'),
	(34,1,'XII IPA 5','2020-05-05 13:57:12','2020-05-05 13:57:12'),
	(35,1,'XII IPA 6','2020-05-05 13:57:12','2020-05-05 13:57:12'),
	(36,1,'XII IPA 7','2020-05-05 13:57:12','2020-05-05 13:57:12'),
	(37,1,'XII IPA 8','2020-05-05 13:57:12','2020-05-05 13:57:12'),
	(38,1,'XII IPA 9','2020-05-05 13:57:12','2020-05-05 13:57:12'),
	(39,1,'XII IPS 1','2020-05-05 13:57:18','2020-05-05 13:57:18'),
	(40,1,'XII IPS 2','2020-05-05 13:57:18','2020-05-05 13:57:18'),
	(41,1,'XII IPS 3','2020-05-05 13:57:18','2020-05-05 13:57:18'),
	(42,1,'XII IPS 4','2020-05-05 13:57:18','2020-05-05 13:57:18'),
	(43,1,'XII IPS 5','2020-05-05 13:57:18','2020-05-05 13:57:18'),
	(44,1,'XII IPS 6','2020-05-05 13:57:18','2020-05-05 13:57:18');

/*!40000 ALTER TABLE `startschool_grade_grades` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table startschool_student_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `startschool_student_users`;

CREATE TABLE `startschool_student_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `nisn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table startschool_user_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `startschool_user_users`;

CREATE TABLE `startschool_user_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
